const { execSync, spawnSync } = require('child_process')
const { readFileSync } = require('fs')
const glob = require('glob')
const cheerio = require('cheerio')

if (!process.env.SUBDIR) {
  console.warn('Use e.g. "SUBDIR=tourismus1 npm start" to only process specific images')
}

console.log('run hugo')
execSync('hugo')

console.log('search image files')
const imageFiles = glob.sync('static/img/' + (process.env.SUBDIR || '**') + '/*')

console.log('search html files')
const htmlFiles = glob.sync('public/**/*.html')

console.log('find img tags')
const imgTags = [] /* { src, dimensions; {width?, height?} } */
htmlFiles.forEach((page) => {
  const $ = cheerio.load(readFileSync(page))
  
  $('img').each((_, img) => {
    const $img = $(img)
    
    const src = normalizeImageSource(decodeURIComponent($img.attr('src')))
    const width = parseOptInt($img.attr('width'))
    const height = parseOptInt($img.attr('height'))
    
    imgTags.push({ src, width, height, page })
  })
})
// console.log(JSON.stringify(imgTags, null, 2))

console.log('analyze image usages')
const imgSources = []
imgTags.forEach(({ src }) => {
  if (imgSources.indexOf(src) === -1) imgSources.push(src)
})
const imgUsages = [] /* { src, pages, maxWidth?, maxHeight? } */
imgSources.forEach((src) => {
  if (imageFiles.indexOf(src) === -1) {
    console.warn('ignoring ' + src)
    return
  }
  
  const matchingTags = imgTags.filter((item) => item.src === src)
  const pages = []

  let maxWidth = matchingTags[0].width
  let maxHeight = matchingTags[0].height
  
  matchingTags.forEach((tag) => {
    if (pages.indexOf(tag.page) === -1) pages.push(tag.page)

    if (tag.width && maxWidth) { maxWidth = Math.max(tag.width, maxWidth) } else maxWidth = null
    if (tag.height && maxHeight) { maxHeight = Math.max(tag.height, maxHeight) } else maxHeight = null
  })
  
  imgUsages.push({ src, pages, maxWidth, maxHeight })
})
// console.log(JSON.stringify(imgUsages, null, 2))

console.log('resize images')
imgUsages.forEach((image) => {
  let maxResolution = null

  const isThumb = /^static\/img\/thumbnail/.test(image.src)
  const isArticleImage = (!isThumb) && image.pages.filter((path) => /public\/blog\/.*\/index\.html/.test(path)).length === image.pages.length
  const isLogo = image.src === 'static/img/logo-small.png' || image.src === 'static/img/logo.png'

  if (isThumb) { maxResolution = '512x' }
  if (isArticleImage) { maxResolution = (image.maxWidth ? image.maxWidth : 1024) + 'x' + (image.maxHeight ? image.maxHeight : '') }
  
  if (maxResolution) {
    assertSpawnSuccess(spawnSync('mogrify', ['-resize', maxResolution + '>', image.src]))
  } else if (!isLogo) {
    console.warn('no target resolution for ' + image.src)
  }
})

console.log('minify all pngs')
imageFiles.filter((file) => /\.png$/.test(file)).forEach((file) => {
  try {
    assertSpawnSuccess(spawnSync('pngquant', ['--speed', '1', '--ext', '.png', '--force',  '--skip-if-larger', file]))
  } catch (ex) {
    console.warn('pngquant failed for ' + file)
  }
})

console.log('remove metadata')
imageFiles.forEach((file) => {
  assertSpawnSuccess(spawnSync('exiftool', ['-overwrite_original', '-all=', file]))
})

console.log('done')

function normalizeImageSource(source) {
  const index = source.indexOf('img/')
  
  if (index === -1) return source
  
  return 'static/' + source.substring(index)
}

function assertSpawnSuccess(result) {
  if (result.error) throw result.error
  if (result.status) {
    // console.log(result.output.join('\n'))
    throw new Error('process returned with ' + result.status)
  }
}

function parseOptInt(value) {
  if (value) {
    const result = parseInt(value)
    
    if (!Number.isSafeInteger(result)) throw new Error()
    
    return result
  } else {
    return null
  }
}
