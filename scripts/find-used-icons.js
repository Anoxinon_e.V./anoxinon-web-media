const { readFileSync } = require('fs')
const { execSync } = require('child_process')
const glob = require('glob')
const cheerio = require('cheerio')

module.exports = function () {
  console.log('run hugo')
  execSync('hugo')

  console.log('search html files')
  const htmlFiles = glob.sync('public/**/*.html')

  console.log('find icons')
  const usedIcons = []
  htmlFiles.forEach((page) => {
    const $ = cheerio.load(readFileSync(page))
    
    $('i').each((_, span) => {
      const $span = $(span)
    
      const classList = ($span.attr('class') || '').split(' ')
    
      classList.forEach((classItem) => {
        if (/^fa-/.test(classItem)) {
          if (usedIcons.indexOf(classItem) === -1) usedIcons.push(classItem)
        }
      })
    })
  })

  console.log('used icons: ' + usedIcons.join(', '))
  
  return usedIcons
}
