+++
title = "Kontakt"
id = "contact"
+++

### Fragen, Anregungen, Mitmachen?

Wir freuen uns über jegliche Kontaktaufnahme, seien es Fragen, Verbesserungsvorschläge oder allgemeine Anfragen.
Solltest Du Interesse haben mitzuwirken, schaue hier vorbei: [Mitmachen](https://anoxinon.de/mitmachen/)  
Unsere Kommunikationskanäle haben wir weiter unten aufgelistet.

**Kontakt via E-Mail:**

Redaktion - Leitung: <redaktion@anoxinon.de>  
GPG Key: [0xB6B60FEF45680EF7C30F6DF532BB82A41E529250](/keys/0xB6B60FEF45680EF7C30F6DF532BB82A41E529250.txt) |
ID: B6B6 0FEF 4568 0EF7 C30F  6DF5 32BB 82A4 1E52 9250

**Kontakt via Fediverse:**

Mastodon: <a href="https://social.anoxinon.de/@anoxinonmedia">@AnoxinonMedia@social.anoxinon.de</a>

**Kontakt via XMPP:**

XMPP MUC: <a href="xmpp:anoxinon@conference.anoxinon.me?join">anoxinon@conference.anoxinon.me</a>

#### Achtung: Anfragen zu dem Verein Anoxinon e.V. oder dessen anderen Dienste sind bitte [direkt](https://anoxinon.de/kontakt/) an diesen zu richten.
