---
title: Linux
layout: wikipage
---   

Linux ist der Oberbegriff für eine Reihe von oft quelloffenen Betriebssystemen, die alle die gleiche Basis haben - den **Linux-Kernel**. Die Wikikategorie sammelt nützliches Wissen rund um diese Betriebssysteme.