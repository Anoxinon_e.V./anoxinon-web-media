---
title: Pacman-Paketmanager
layout: wikipage
---   

Der **Pacman** ist eine Software um Pakete ([Was ist ein Paket?](/anxicon/#paket-linux)) unter Linux verwalten.  Der Oberbegriff nennt sich [Paketmanager](/wiki/betriebssysteme/linux/paketmanager). Folgend wollen wir einige nützliche Tipps zu pacman geben. Wir beschreiben hier die Kommandozeilenschnittstelle, [es gibt allerdings auch grafische AppStores dafür](/wiki/betriebssysteme/linux/paketmanager).

**Legende**

Bei der Dokumentation der Kommandozeile werden überlicherweise einige Platzhalterzeichen verwendet. Diese musst du als Nutzender an dein Szenario anpassen. Folgend eine Auflistung.

- `<DeinText>` &rarr; dort lässt du die spitzen Klammern weg und schreibst stattdessen den von dir gewünschten Text
- `user$` &rarr; steht für die Prompt der Kommandozeile, das musst du nicht eingeben


**System aktualisieren**

```bash
user$ sudo pacman -Syu
```

**Pakete installieren**

```bash
user$ sudo pacman -S <paket>
```