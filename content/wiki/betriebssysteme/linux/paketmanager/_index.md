---
title: Paketmanager
layout: wikipage
---   

Ein Paketmanager ist ein Programm zur Softwareverwaltung unter Linux. Anders gesagt ist es der **AppStore unter Linux**. 
Mit dem Paketmanager verwaltet man standardmäßig **alle** Software auf seinem Linux-System. Im Gegensatz zu Windows und MacOS kann man also jede Software sowie das Betriebssystem selbst von zentraler Stelle installieren und aktualisieren, meist mit ein- oder zwei Befehlen/Klicks.

Es gibt viele verschiedene Paketmanager, welcher zum Einsatz kommt hängt von der jeweiligen Distribution ab. 

**Aufgaben des Paketmanagers**

- Softwareinstallation
- Softwareaktualisierung
- Softwaredeinstallation

**Nutzung des Paketmanagers**

Für die Nutzung von Paketmanagern kann man entweder auf die Kommandozeilenschnittstelle des jeweiligen Paketmanagers zurückgreifen, oder man verwendet einen grafischen AppStore. Beides kommt meist von Haus aus mit jedem Linux-System mit, man hat also die freie Wahl.

**Auflistung von Paketmanagern**

Folgend eine Auflistung der bekannten Paketmanager. 

- [APT](/wiki/betriebssysteme/linux/apt-paketmanager/) (Debian, Ubuntu, Linux Mint, MX Linux, ...)
- DNF (Fedora)
- YUM (Red Hat Enterprise Linux, CentOS, AlmaLinux, RockyLinux...)
- Zypper (OpenSUSE)
- Pacman (ArchLinux, Manjaro)

**Auflistung von grafischen AppStores**

Folgend einige grafische AppStores für die oben genannten Paketmanager.

- GNOME Software
- Synaptic (nur für APT!)