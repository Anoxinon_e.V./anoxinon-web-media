---
title: APT-Paketmanager
layout: wikipage
---   

Das **Advanced package tool**, kurz **APT** ist eine Software um Pakete ([Was ist ein Paket?](/anxicon/#paket-linux)) unter Linux verwalten. Der Oberbegriff nennt sich [Paketmanager](/wiki/betriebssysteme/linux/paketmanager). Folgend wollen wir einige nützliche Tipps zu APT geben. Wir beschreiben hier die Kommandozeilenschnittstelle, [es gibt allerdings auch grafische AppStores dafür](/wiki/betriebssysteme/linux/paketmanager).

**Legende**

Bei der Dokumentation der Kommandozeile werden überlicherweise einige Platzhalterzeichen verwendet. Diese musst du als Nutzender an dein Szenario anpassen. Folgend eine Auflistung.

- `<DeinText>` &rarr; dort lässt du die spitzen Klammern weg und schreibst stattdessen den von dir gewünschten Text
- `user$` &rarr; steht für die Prompt der Kommandozeile, das musst du nicht eingeben

**System aktualisieren**

```bash
user$ sudo apt update && sudo apt upgrade
```

**Aktuelle Paketliste herunterladen**

```bash
user$ sudo apt update
```

**Alle Pakete aktualisieren**

Bitte **immer** die Paketliste aktualisieren, bevor du die Pakete an sich aktualisierst!!!

```bash
user$ sudo apt upgrade
```

**Pakete installieren**

Bitte **immer** das System auf den aktuellen Stand bringen, bevor du neue Pakete installierst!

```bash
user$ sudo apt install <paket>
```