---
title: Wiki
layout: wikipage
---

Willkommen im neuen Anoxinon Media Wiki des Anoxinon e.V.. Hier finden sich Informationen zu den verschiedensten Themenfeldern des Anoxinon e.V., übersichtlich gegliedert und verständich aufbereitet. Dazu zählen unter anderem:

- Informationen zu Betriebssystemen, zu Linux und Windows
- Inhalte und Ressourcen zu der Kopfthematik des Datenschutzes, mit Flyern für Kinder und Studien zu Google
- Listen von Diensten, von E-Mail bis zu Videochat
- HowTos und Tipps für Kommandozeilen-Tools und Android
- Infoquellen, die wir Anoxianer:innen über die Zeit zu lieben und zu schätzen gelernt haben, von Büchern bis zu Wikis
- Open-Source Programme für die Workstation bis zum Smartphone

## Ziel

**Die Konsolidierung von jedweden nützlichen Inhalten rund um die Vereinsthematiken!**

Wir möchten alles Wissen in und rund um unseren Verein, das irgendwie nützlich sein könnte - egal wie groß oder klein - geordnet, zielführend und auffindbar zugänglich machen. Das Wiki soll die Plattform werden, auf der das alles geteilt werden kann, damit es nicht in der Senke verschwindet, sondern öffentlich verfügbar einen Nutzen entfalten kann. Das heißt auch dein Wissen ist hier mehr als Willkommen - read on!

# Navigation

Die Navigation befindet sich in der rechten Seitenleiste. Dort finden sich verschiedene Kategorien, die man durchblättern kann. Je nach Unterkategorie werden dabei weitere Punkte sichtbar und aufgeklappt. Die eigentlichen Inhalte sind dann auf der linken Seite - so wie dieser Text - ersichtlich. Verlinkungen im Text selber verweisen zu weiterführenden Ressourcen - das können weitere Wikiseiten wie auch externe Quellen sein.

## Beitragen

Das Anoxinon Media Wiki wurde erschaffen, um der Gemeinschaft einen Platz zu bieten, ihr geballtes Wissen einfach, schnell und sichtbar mit anderen zu teilen. Die Pflege der Inhalte passiert in einem [öffentlich einsehbaren Git-Repositorium auf Codeberg](https://codeberg.org/Anoxinon_e.V./anoxinon-web-media). **Jede und jeder ist eingeladen, etwas beizutragen, Verbesserungen vorzuschlagen und Änderungen einzureichen!** Ob per Issue, Pull-Request, E-Mail oder Fediverse - die Community dankt dir jeden noch so kleinen, nützlichen Inhaltsschnipsel, der für die Zukunft gesammelt werden kann.

## Work in Progress

Das Wiki wurde von uns ins Leben gerufen, um die bisher auf der Anoxinon Media Webseite verstreuten Informationen abseits der Blogbeiträge zu konsolidieren. Es wurde dabei vor allem die bisher bestehende Extra-Sektion ausgemustert und die Inhalte neu geordnet in dieses Wiki überführt. Das bedeutet, dass das Wiki ein **Work in Progress** ist - in der digitalen Welt sind Veränderungen rapide und auch unsere Extras waren nicht mehr durchweg aktuell. Hilf uns doch, das Wiki aktuell zu halten, indem du etwas beiträgst! Im Idealfall stellen wir uns das Wiki als Gemeinschaftsprojekt zwischen den Mitgliedern des Anoxinon e.V. und unserer Gemeinschaft vor!