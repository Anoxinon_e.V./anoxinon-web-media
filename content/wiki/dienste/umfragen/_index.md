---
title: (Termin-) Umfragen
layout: wikipage
---

Für Umfragen un die Terminvereinbaung gibt es inwzsichen einige Dienste. Ein paar Beispiele listen wir folgend auf.

## Framadata

Eine Software für die Terminverwaltung und Umfragendurchführung ist Framadata, entwickelt von der französischen Organisation Framasoft.

- [Nuudel](https://nuudel.digitalcourage.de/) - Framadate-Instanz von Digitalcourage e.V. ([Datenschutzerklärung](https://digitalcourage.de/datenschutz-polls), [Verantwortliche:r](https://digitalcourage.de/kontakt)) 
- [Disroot Poll](https://poll.disroot.org/) - Disroots Framadate-Angebot ([Datenschutzerklärung](https://disroot.org/de/privacy_policy), [Verantwortliche:r](https://disroot.org/de/privacy_policy)) 
- [Snopyta Poll](https://poll.snopyta.org/) - Terminvereinbarung mithilfe von Framadate bei Snopyta ([Datenschutzerklärung](https://snopyta.org/privacy_policy/), [Verantwortliche:r](https://snopyta.org/legal/))

## Sonstiges

Neben Framadate gibt es aber auch noch weitere Dienste, die datenschutzfreundlich Terminvereinbarung und Umfragen anbieten:

- [Dudle](https://dudle.inf.tu-dresden.de/) - Tool von der TU Desden ([Datenschutzerklärung](https://dudle.inf.tu-dresden.de/about.cgi), [Verantwortliche:r](https://dudle.inf.tu-dresden.de/about.cgi)) 
- [Systemli Croodle](https://systemli.org/poll/) - Croodle Umfragen bei Systemli ([Datenschutzerklärung](https://www.systemli.org/tos/), [Verantwortliche:r](https://www.systemli.org/support-us/))


Eine [Anleitung zur Nutzung von Duudle](https://fsi.spline.de/wiki/index.php/Dudle_erstellen).
**Bitte beachten, dass man auch einen Alias-Namen eingeben kann, egal wie gut der Datenschutz ist. Der volle Name hat für die Terminvereinbarung funktional keinen Mehrwert.**  

Alternativ kann man, sofern man eine [Nextcloud Instanz](/wiki/dienste/cloud/) besitzt, die App "Polls" verwenden.
