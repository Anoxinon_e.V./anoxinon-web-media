---
title: Navigation
layout: wikipage
---

## OpenStreetMap

OpenStreetMap ist ein Gemeinschaftsprojekt, das frei nutzbare Geodaten sammelt und offen zur Verfügung stellt. Inzwischen gibt es eine Vielzahl von Projekten, die diese Daten für Navigations- und Kartenanwendungen nuzuen.

OpenStreetMap bietet viele individuelle Möglichkeiten z.B. für Rollstuhlfahrer, um einen Briefkasten, eine Bushaltestelle zu finden, Briefkästen, Wanderwege, Fahrradwege und ganz klassisch um mit dem Auto oder zu Fuß schnell von A nach B zu kommen.   

## Karte- und Navigation im Browser

- [Metager Maps](https://maps.metager.de/map/) - Karte und Navigation des SUMA-EV, auf Basis von OpenSreetMap ([Datenschutzerklärung](https://metager.de/datenschutz), [Verantwortliche:r](https://metager.de/impressum))
- [OpenStreetMap](https://openstreetmap.org) - OpenStreetMap Projekt ([Datenschutzerklärung](https://wiki.osmfoundation.org/wiki/Privacy_Policy), [Verantwortliche:r](https://wiki.osmfoundation.org/wiki/About))
- [OpenStreetMap - Deutschland](https://openstreetmap.de) - Deutsches OpenStreetMap Projekt ([Datenschutzerklärung](https://www.fossgis.de/datenschutzerkl%C3%A4rung/), [Verantwortliche:r](https://openstreetmap.de/impressum.html))
- [Qwant Maps](https://www.qwant.com/maps/) - Karte und Navigation des Qwant-Projekts, auf Basis von OpenStreetMap ([Datenschutzerklärung](https://about.qwant.com/de/rechtlich/datenschutzrichtlinie/), [Verantwortliche:r](https://about.qwant.com/de/rechtlich/impressum/))

