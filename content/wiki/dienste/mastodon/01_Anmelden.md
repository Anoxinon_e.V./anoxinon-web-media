---
title: 1. Registrieren
layout: wikipage
--- 

Wenn Du dich bei Twitter anmeldest, dann hast Du dich quasi unbewusst für Twitter als deinen Provider entschieden, deshalb hast Du dann dort auch eine Sozial-Media-Adresse mit Twitter im Namen
(**@DeinName@Twitter.com**).

Wenn Du dich bei Mastodon anmelden willst, dann musst Du dir erst Mal einen Provider aussuchen (bei eMail ist das ja auch nicht anders).
Eigentlich ist Provider hier aber nicht der richtige Begriff, denn die Instanzen werden bei Mastodon meist von Einzelpersonen oder kleinen Gemeinschaften betrieben.
Es gibt Instanzen mit ein paar wenigen Accounts und welche mit hunderttausenden.
Bei den kleinen Gemeinschaften kannst Du vermutlich mehr beeinflussen wie der Umgang miteinander funktioniert (muss aber nicht so sein).
Bei den großen Instanzen (mit großen Servern) hast Du sehr wahrscheinlich eine stabilere Infrastruktur, da sich mehr Menschen um den technischen Betrieb kümmern.

Hier ein paar Möglichkeiten, die dich bei der Sucher deiner neuen Instanz unterstützen können

- [Joinmastodon](https://joinmastodon.org/): Das ist die offizielle Übersicht von Mastodon zu eingen ausgewählten Instanzen.
- [Instances.social](https://instances.social/): Dieses Tool versucht dich interaktiv zu einer Auswahl zu führen.
- [Fediverse.observer](https://fediverse.observer/): Interaktive Seite zum Suchen nach Instanzen
- [mastodon-near-me](https://umap.openstreetmap.fr/en/map/mastodon-near-me_550053#3/18.98/29.53): Instanzen auf einer Karte suchen
- [Deutschsprachige Instanzen](https://contentnation.net/de/favstarmafia/artikel2): Liste mit Instanzen auf denen auch Deutsch geschrieben wird
- [Unmung.com](http://www.unmung.com/mastoview?url=mastodonten.de&view=local): Hier kannst Du in die Beiträge einer Instanz schauen
- [communitywiki](https://communitywiki.org/trunk): Accounts nach Kategorien

Wenn Du dir eine Instanz heraus gesucht hast, meldest Du dich dort über den Webclient an, der automatisch geöffnet wird, wenn Du die Seite (URL) der Instanz aufrufst.
Danach hast dann einen Accountnamen mit der folgenden Struktur: **@MeinName@MeineMastodon.Instanz** (bei uns z.B. *@name@social.anoxinon.de*).

Nicht alle Instanzen lassen zu jeder Zeit Registrierungen zu.
Wenn der Server schon gut ausgelastet ist, kann es sein, dass temporär keine Anmeldung mehr möglich ist.
Dann erlaubt der Admin (der Betreiber der Instanz) oft nur noch die Anmeldung per Einladung.
Du kannst dann versuchen bei einem User der Instanz nach einer Einladung zu fragen, meist können die dann solche Einladungen aussprechen.

**WICHTIG:** Mastodon verlangt immer mal wieder eine PIN für die 2-Faktor-Authentifizierung, zum Beispiel wenn man sich auf einem neuen Gerät anmeldet.
Deshalb solltet du für die Registrierung keine temporären Mailsdressen verwenden.

