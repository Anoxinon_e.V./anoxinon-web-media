---
title: 2. Benutzen
layout: wikipage
--- 

# Anderen folgen und mehr..
Wenn Du anderen folgen willst, denn musst Du nicht nur nach ihrem Namen suchen (wie bei Twitter), sondern auch nach ihrer Instanz.
Das kann schon mal verwirrend sein, denn es kann den gleichen Namen auf vielen Instanzen geben.
Ich habe die Erfahrung gemacht, dass Clients nur nach den lokalen Accounts suchen.
Am besten suchst Du nach neuen Account über den WebClient, der findet zumindest bei mir wesentlich mehr Accounts in anderen Instanzen.

**Einer anderen Instanz folgen**\
Bei einigen Clients (z.B. Fedilab für Android) gibt es eine ziemlich coole zusätzliche Funktion.
Damit kann man sich die öffentliche Local Timeline  einer anderen Instanz anzeigen lassen (so wie die eigene Local Timeline).

**Warum sehe ich nicht alle Follower / Followings eines Accounts?**\
Wenn man sich in meinem Client die Follower eines anderen Accounts ansehen will, dann sieht man erst mal nur die Accounts auf der eigenen Instanz.
Im Webclient gibt es dafür die Option \[Vollständiges Profil anzeigen\], hier kann man dann auch den Accounts von anderen Instanzen folgen.
Hierzu muss man leider immer mal wieder seinen eignen (vollständigen) Accountnamen eingeben.

**Listen**\
Bei Mastodon kann man nur die Accounts in eine Liste stecken, denen man auch folgt.
Man kann also nur Teilmengen der eigenen Timeline  in Listen abbilden (also anders als bei Twitter).
Das wird sich vermutlich wegen der Föderation auch niemals ändern.
Als Alternative kann man ganz allgemein immer mit Listen arbeiten.
Man folgt einfach allen die man interessant findet und sortiert sie dabei gleich in eine passende Liste ein.

TIPP: Falls ihr ein paar Accounts habt, die euch besonders wichtig sind, die aber nur relativ selten was schreiben und deshalb in der Timeline  untergehen, dann legt euch doch speziell für die eine Liste an, dann könnt ihr immer mal wieder da rein schauen ob es von denen was neues gibt.

**Mute: Stummschalten**\
Mit Mute kann kann man einem Account dem man folgt in der Timeline ausblenden.

Aktion: A mutet B

- A sieht nur noch die Antworten (Mention) von B.\
- B erfährt davon nichts.

Dazu müsst ihr auf den Account der Person gehen und dann bei den drei Punkten „stummschalten“ auswählen.

**Mute Boosts**\
Zusätzlich zu Mute gibt es noch die Funktion „Geteilte Beiträge von verbergen“ (das können nicht alle Clients, der Webclient kann es aber).
Damit werden nur die Boosts (Retweets) des Accounts unterdrückt, alles andere könnt ihr noch sehen.

**Blocking: blockieren**\
Das Blockieren ist die verschärfte Variante von Mute, dabei passiert folgendes\

Aktion: A blockt B

- A kann B nicht mehr sehen (auch keine Nachrichten).
- B kann A keine Nachrichten mehr senden.
- B kann A nicht mehr teilen.
- B wird als Follower von A gelöscht.
- B erfährt davon nichts (eigentlich schon, weil er dir nicht mehr folgt).
- B meldet sich als C an und alles war für die Katz …

# Einen Post schreiben
Du kannst sofort nach dem Anmelden einen Post schreiben, der kann dann von jedem gelesen werden, dazu braucht man noch nicht einmal einen Account.
Bei Mastodon ist so ein Post dann „Öffentlich“ (Public).
Bei Twitter gibt es nur Public und Direkt, bei Mastodon gibt es aber auch noch Nicht gelistet (unlistet) und Nur Folgende (Privat).
Du kannst deshalb bei jedem Post (meist rechts neben dem Symbol für die Bilder), die Sichtbarkeit mit der er veröffentlicht werden soll festlegen.

- **Öffentlich (Public)**: Absolut jeder kann es sehen.\
- **Nicht gelistet (Unlistet)**: Mann kann es überall sehen, außer in der öffentlichen Timeline.\
- **Nur Folgende (Privat)**: Nur deine Follower können es sehen.\
- **Direkt (Direct)**: Nur der Empfänger kann es sehen (wie die DM bei Twitter).

[Hier](https://docs.joinmastodon.org/user/posting/#privacy) steht das Ganze noch mal im Detail auf der Seite von Mastodon.

# Direktnachrichten

**ACHTUNG: Direkt ist nicht privat**

Direktnachrichten haben bei Mastodon den Charakter einer eMail. Alles was ihr nicht per Mail versenden würdet, solltet ihr deshalb auch nicht per DM versenden.

Wir empfehle für vertrauliche Nachrichten deshalb einen Account bei Matrix oder XMPP anzulegen und den in der BIO bei Mastodon zu hinterlegen.

**Direktnachrichten erkennen**

Im Webclient kann man sich sich eine extra Spalte für die Direktnachrichten erstellen.
Dazu geht man auf die Spalte ganz rechts, die mit „Erste Schritte“ überschrieben ist.
Dort klickt man auf den Eintrag “ Direktnachrichten“.
Jetzt gibt es eine neue Spalte mit den Direktnachrichten.
Wenn man diese Spalte immer sehen will, muss man sie noch „anheften“.
Nachdem man sie angeheftet hat, kann man sie dann da hin verschieben, wo sie zukünftig stehen soll.

**Direktnachrichten an andere Netzwerke**

Man kann mit Mastodon auch Accounts von anderen Netzwerken aus dem Fediverse folgen (z.B. Friendica, GNU Social, Pleroma, …), die das gleiche Protokoll verwenden (ActivityPub).
Wenn man solchen Accounts eine Nachricht schreibt, sollte man immer damit rechnen, dass die Vertraulichkeitsstufen von Mastodon nicht berücksichtigt werden.
Ich habe z.B. einen Post als „direkt“ an einen Friendica Account geschrieben, der dann in Friendica öffentlich (global) sichtbar war.

Accounts aus anderen Netzwerken kann man leider nur sehr kompliziert erkennen.
Man muss im Webclient auf den Account klicken und dann noch einmal auf  „Vollständiges Profil anzeigen“, dann bekommt man den Account im eigentlichen Netzwerk angezeigt.

# Die Time-Line und ihre Struktur

Mastodon ist ein Netzwerk von Instanzen die miteinander verbunden sind (nicht jede Instanz ist mit jeder Verbunden, das legt das jeweilige Verhalten der Benutzer der Instanz fest).
Deshalb gibt es auch verschiedenen Ebenen auf denen man etwas angezeigt bekommt.

- **Home**: Ist genau das was Du von Twitter schon kennst, hier siehst Du die Nachricht von den Accounts, denen Du folgst.
- **Local**: Hier siehst Du was die Accounts schreiben, die auf der gleichen Instanz angemeldet sind wie Du.
- **Föderation**: Hier stehen all die Trööts, die in all den Instanzen geschrieben werden, die mit deiner Instanz verbunden sind, weil Accounts von deiner Instanz, Accounts von anderen Instanzen folgen, oder mit ihnen interagieren.

Bei Mastodon (eigentlich im ganzen Fedivers) bekommt man die Posts genau in der Reihenfolge angezeigt, in der sie von deinen Followings geschrieben werden, eine Manipulation oder Sortierung der Inhalte (wie zum Beispiel bei Twitter oder Facebook) findet nicht statt.


