---
title: Desktopverknüpfungen (Linux)
layout: wikipage
---

Es kommt vor, dass man Shellskripte baut und ein Nicht-Techniker diese starten soll.
Oder man hat eine Anwendung an der Paketverwaltung vorbei installiert, aber will diese
Anwendung dennoch starten. Die Lösungen sind Desktopverknüpfungen bzw. ``.desktop``-Dateien.
Diese können natürlich auch an anderen Stellen verwendet werden, mit denen man in der
grafischen Oberfläche Anwendungen startet, wie z.B. in der Gnome Shell.

## Pfade

| Dateityp        | global                      | aktueller Benutzer              |
|-----------------|-----------------------------|---------------------------------|
| Desktop-Dateien | ``/usr/share/applications`` | ``~/.local/share/applications`` |
| Symbole         | ``/usr/share/icons``        | ``~/.local/share/icons``        |

## Beispiel

```
[Desktop Entry]
Type=Application
Name=Anoxinon-Entspannung
Comment=15 Sekunden, in denen man ein leeres Terminal ansehen kann
Exec=sleep 15
Terminal=true
Icon=anoxinon-logo
```

Pfad für die Datei: ``~/.local/share/applications/anoxinon-sleep.desktop``  
Pfad für das Logo: ``~/.local/share/icons/hicolor/256x256/apps/anoxinon-logo.png``

Hinweise zum Symbol:

- ``hicolor`` ist das Fallback wenn das aktuelle Theme keinen Eintrag hat
- es ist möglich, mehrere Auflösungen zu hinterlegen, aber eine Auflösung ist bereits ausreichend

## Links

- <https://wiki.archlinux.org/title/desktop_entries>
- <https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html>
