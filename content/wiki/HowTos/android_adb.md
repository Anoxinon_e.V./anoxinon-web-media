---
title: Android & ADB
layout: wikipage
---


## APKs extrahieren per ADB

Apps auflisten: ``adb shell pm list packages``

Pfad einer App ausgeben: ``adb shell pm path org.fdroid.fdroid``

eine APK-Datei vom Gerät holen: ``adb pull /data/app/org.fdroid.fdroid-0kUWuWhMcm5jLrQsyNNKhg==/base.apk .``
