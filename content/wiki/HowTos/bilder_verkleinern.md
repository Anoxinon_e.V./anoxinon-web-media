---
title: Bilder verkleinern
layout: wikipage
---

Ein Problem, das auch wir bei Anoxinon haben: Hochauflösende Bilder sind schön, aber
nicht immer passend. Man kann sich natürlich mit Gimp helfen, aber es gibt bequemere
Lösungen:

### mogrify -resize

mogrify ist ein Teil von ImageMagick und wird [hier](https://imagemagick.org/script/mogrify.php)
ausführlich beschrieben. Eine Beschreibung der Optionen zur Größenänderung gibt es
[hier](https://www.imagemagick.org/script/command-line-processing.php#geometry).

Beispielaufruf: ``mogrify -resize 512x\> *.png`` - damit bekommen alle Bilder im aktuellen Ordner
mit der Endung .png eine Breite von 512 Pixeln, wenn diese davor größer waren.

Das ``\>`` sorgt dafür, dass Bilder nur verkleinert werden (und nicht vergrößert werden). Ein ``>`` würde
reichen, aber weil es die Shell sonst als Ausgabeumleitung interpretieren würde, muss man es escapen.

Mit ``x512`` anstelle von ``512x`` könnte man die Höhe angeben.

Es ist auch möglich, die Breite und Höhe anzugeben, also ``512x1024``. Dann gelten beide
Größenangaben als Obergrenze - das Seitenverhältnis wird beibehalten.

Wenn man das Seitenverhältnis (warum auch immer) ändern will, dann hängt man ``!`` an -
also z.B. ``512x1024!``.

Man kann die Skalierung auch relativ zur aktuellen Größe machen - ``mogrify -resize 50% *.png``.
Da sollte man aber aufpassen, dass das bei jedem Aufruf erneut verkleinert wird.

Dieses Werkzeug gibt es in den gängien Paketquellen, wie z.B. bei [Debian](https://packages.debian.org/buster/imagemagick).

### pngquant

Ganz kurz: ``pngquant --speed 1 --ext .png --force --skip-if-larger *.png``

Damit werden alle png-Dateien im aktuellen Ordner durch die optimierte Version ersetzt, sofern diese kleiner ist.

Auch dieses Werkzeug gibt es in den gängien Paketquellen, wie z.B. bei [Debian](https://packages.debian.org/buster/pngquant).

### exiftool

Kombiniert mit find: ``find . -name '*.jpg' -or -name '*.png' -exec exiftool -overwrite_original -all= {} \;``

Damit wird es für alle jpg- und png-Bilder im aktuellen Ordner und in den Unterordnern ausgeführt. 

Hier ist der Paketname nicht so naheliegend. Bei Debian ist es [libimage-exiftool-perl](https://packages.debian.org/buster/libimage-exiftool-perl).

Die Größeneinsparung dadurch ist minimal, da man nur Metadaten entfernt.
Das steht in dieser Rubrik, weil es hier inhaltlich gut passt.

