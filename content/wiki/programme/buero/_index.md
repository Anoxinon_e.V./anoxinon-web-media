---
title: Büro
layout: wikipage
--- 

## Office-Pakete

- [LibreOffice (Windows, MacOS, Linux)](https://libreoffice.org/LibreOffice)  

## PDF-Anzeige

- [Okular (Windows, Linux)](https://okular.kde.org/Okular)
- [Evince (Linux)](https://wiki.gnome.org/Apps/Evince)  
- [muPDF (Windows, Linux, Android)](https://f-droid.org/packages/com.artifex.mupdf.viewer.app/)  

## Mindmaps

- [Freeplane (Windows, MacOS, Linux)](https://freeplane.org/wiki/index.php/HomeFreeplane)  

## Scanner

- [SimpleScan (Linux)](https://wiki.ubuntuusers.de/Simple_Scan/SimpleScan)  

## Notizen

- [RedNotebook (Windows, MacOS, Linux)](https://wiki.ubuntuusers.de/RedNotebook)
- [Tasque (Linux)](https://wiki.gnome.org/Attic/TasqueTasque)
- [Aikee (Windows, Linux)](https://github.com/rockiger/akiee) 

## Karteikarten

- [Anki (Windows, MacOS, Linux, Android, iOS](https://apps.ankiweb.net/Anki)  

## Archiv

- [Xarchiver (Linux)](https://en.wikipedia.org/wiki/Xarchiver)
- [Ark (Linux)](https://kde.org/applications/utilities/ark) 

## CD/DVD brennen

- [K3B (Linux)](https://wiki.ubuntuusers.de/K3b)
- [Brasero (Linux)](https://wiki.gnome.org/Apps/Brasero)  

## Finanzen
 
- [Hibiscus (Windows, Linux, MacOS)](https://willuhn.de/products/hibiscus)  

## Dateiverwaltung

- [Amaze (Android)](https://f-droid.org/en/packages/com.amaze.filemanager)  

## Kalender

- [SimpleCalender (Android)](https://f-droid.org/en/packages/com.simplemobiletools.calendar.pro)  
