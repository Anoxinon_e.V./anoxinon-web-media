---
title: Kommunikation
layout: wikipage
--- 


## E-Mail

### Grafische Programme

- [Thunderbird (Windows, Linux, MacOS)](https://thunderbird.net)
- [Claws Mail (Windows, Linux)](/wiki/programme/kommunikation/claws-mail/)  
- [K9 Mail (Android)](https://f-droid.org/packages/com.fsck.k9/)
- [FairEmail (Android)](https://f-droid.org/en/packages/eu.faircode.email/)  

### Kommandozeilen-Programme (CLI/TUI)

- [neomutt (Windows, Linux, MacOS)](https://neomutt.org/)

## XMPP

- [Gajim (Windows, Linux, MacOS)](https://gajim.org/Gajim)
- [Dino (Linux)](https://dino.im)  
- [Conversations (Android)](/wiki/programme/kommunikation/conversations/)
- [blabber.im (Android)](https://f-droid.org/en/packages/de.pixart.messenger)  

## Matrix

- [Element (Windows, Linux, MacOS, Android, iOS)](https://element.io)  

## VoIP
 
- [Mumble (Windows, Linux, MacOS)](https://mumble.info)
- [Plumble (Android)](https://f-droid.org/en/packages/com.morlunk.mumbleclient) 
