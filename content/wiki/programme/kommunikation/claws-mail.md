---
title: Kommunikation
layout: wikipage
--- 

[Claws Mail](https://claws-mail.org) ist ein quelloffenes E-Mail Programm. 

## Claws Mail unter Linux installieren

Claws Mail ist weit verbreitet und in den meisten Linux Distributionen verfügbar. Suche einfach nach Claws Mail im [AppStore](/wiki/betriebssysteme/linux/paketverwaltung/) deines Linux-Systems. Wahlweise via grafischer Oberfläche oder Kommandozeile.

1. Das Paket `claws-mail` installieren.

https://www.microsoft.com/de-de/download/details.aspx?id=26999

## Claws Mail unter Windows installieren

Die Installation läuft in etwa wie bei jedem anderen Windows Programm. Einzig ein Microsoft Visual C++ Servicepack muss manuell als Abhängigkeit installiert werden:

1. Lade die passende Version von Claws Mail [von deren Webseite](https://claws-mail.org/win32/) herunter (achte auf die Version 4.0!)
2. Lade das [Microsoft Visual C++ Servicepack](https://www.microsoft.com/de-de/download/details.aspx?id=26999) herunter, das für Claws Mail benötigt wird
3. Installiere das Servicepack
4. Installiere Claws Mail

{{< bildstrecke claws-mail_installation_windows >}}


## Claws Mail einrichten

Beim ersten Start wird man interaktiv durch die Claws Mail Einrichtung geführt.

{{< bildstrecke claws-mail_einrichtung >}}