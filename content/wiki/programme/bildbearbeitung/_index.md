---
title: Bildbearbeitung
layout: wikipage
--- 

## Allgemein (Photoshop-Alternativen)

- [Gimp (Windows, MacOS, Linux)](https://gimp.org)
- [Krita (Windows, MacOS, Linux)](https://krita.org) 

## RAW-Bearbeitung (Lightroom-Alternativen)

- [Darktable (Windows, MacOS, Linux)](https://darktable.org)
- [RAW-Therapee (Windows, MacOS, Linux)](https://rawtherapee.com/)  

## Vektorgrafiken

- [Inkscape (Windows, MacOS, Linux)](https://inkscape.org)

## Bildverwaltung

- [Shotwell (Linux)](https://wiki.ubuntuusers.de/Shotwell) 

## 3D-Animationen

- [Blender (Windows, MacOS, Linux)](https://blender.org)  

## Flyer

- [Scribus (Windows, MacOS, Linux)](https://scribus.net)  

## Panorama

- [Hugin (Windows, MacOS, Linux)](https://wiki.ubuntuusers.de/Hugin)  
