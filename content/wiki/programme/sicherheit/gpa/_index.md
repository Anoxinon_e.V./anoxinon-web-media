---
title: GPA
layout: wikipage
--- 

GPA steht für GNU Privacy Assisant. Es ist ein grafisches Programm, um den eigenen OpenPGP-Schlüsselbund zu verwalten und ergänzt damit das Kommandozeilenprogramm GPG (GNU Privacy Guard). Auf dieser Wikiseite findest du Tipps & Tricks zu Verwendung. Für Hintergrundinformationen ließ bitte unsere [OpenPGP-Artikelserie](https://anoxinon.media/blog/pgp1/).

## GPA unter Linux installieren

GPA ist weit verbreitet und daher in den meisten Linux-Distributionen von Haus aus enthalten, bei allen relevanten Distros unter dem Paketnamen `gpa`. Erfahrungsgemäß machen allerdings die Linux-Appstores probleme, wenn der Paketname nur drei Buchstaben hat und auch anhand der Beschreibung findet man GPA nicht immer. Daher empfiehlt sich eine Installation über die Kommandozeile. Für die bekanntesten Paketmanager findest du [eine Beschreibung bei uns im Wiki, wie du das bewerkstelligen kannst](/wiki/betriebssysteme/linux/paketmanager/).

1. Das Paket `gpa` installieren

## GPA unter Windows

Unter Windows lässt sich GPA am einfachsten via GPG4Win installieren. GPG4Win ist eine Sammlung von Windows-Programmen für OpenPGP. 

1. Herunterladen auf [gpg4win.org](https://gpg4win.org/get-gpg4win.html). Die Entwicklenden bitten vor dem Download um eine Spende. Wenn du nichts Spenden möchtest, kannst du einfach $0 anwähln.
2. Den Installer starten, Sprache auswählen und nach eigenen vorlieben durchklicken
3. Beim Schritt "Komponenten auswählen" darauf achten, **GPA** anzuhaken

{{< bildstrecke gpa_installation_windows >}}

## GPA einrichten

Beim ersten Start bekommt man die Möglichkeit, interaktiv ein neues Schlüsselpaar zu erstellen.

{{< bildstrecke gpa_einrichten >}}