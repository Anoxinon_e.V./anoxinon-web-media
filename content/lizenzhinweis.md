+++
title = "Lizenzhinweis"
+++

Mit Beschluss der Mitgliederversammlung vom 24. Februar 2019,  
stehen alle vom Verein erstellten Inhalte, die auf dieser Internetseite verbreitet werden, unter der [Creative Commons Namensnennung 4.0 International Lizenz](https://creativecommons.org/licenses/by/4.0/deed.de).  
Dies dient der besseren Verbreitung und schafft Sicherheit für Nutzer eben dieser.  

**Ausgenommen:**  
Die Verwendung des Logos und zugehörige Grafiken, außerhalb von uns erstellten Inhalten.

Beispiel: Die Präsentation "DSGVO für Profis" beinhaltet unser Logo und zugehörige Grafiken. Wenn Du diese weiter veränderst oder verteilst, stellt das kein Problem dar.
Solltest Du jedoch das Logo aus der Präsentation kopieren und in einer neu erstellten verwenden, ist dies derzeit nicht erlaubt. Für diesen Fall wird ein entsprechendes Pressekit, unter eigener Lizenz, zur Verfügung gestellt.

**Noch Fragen?**  
Schreibt uns doch eine [E-Mail](mailto:postfach@anoxinon.de).
