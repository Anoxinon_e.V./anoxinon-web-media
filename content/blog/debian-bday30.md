---
title: Happy Birthday, Debian!
date: "2023-08-16T08:00:00+02:00"
tags:
- Anfänger
categories:
- Freie Software
banner: "/img/debian/debian-thumbnail.jpg"
description: Debian wird 30 Jahre alt 
---

**Inhaltsverzeichnis:**

1. [Wer ist denn Debian](#wer-ist-denn-debian)
2. [Lust auf mehr bekommen?](#lust-auf-mehr-bekommen)
3. [Von Windows zu Debian GNU/Linux](#von-windows-zu-debian-gnu-linux)
4. [Danke, Danke, Danke!](#danke-danke-danke)

### Wer ist denn Debian?

Das Debian-Projekt ist ein Zusammenschluss von Freiwilligen mit
der Mission ein freies Betriebssystem zu schaffen. Gegründet
wurde das Projekt am 16. August 1993 von Ian Murdock. Weshalb wir
heute, am 16. August 2023, zum 30. Geburtstag gratulieren. 
"Happy Birthday, Debian!"

Wer mehr über die Philosophie und die Geschichte erfahren möchte,
kann sich die folgenden Seiten des Debian Projekts ansehen:

 * [Unsere Philosophie: Warum wir es machen und wie wir es  machen](https://www.debian.org/intro/philosophy)
 * [Eine kurze Geschichte von Debian](https://www.debian.org/doc/manuals/project-history/)

Was macht jetzt Debian so besonders gegenüber anderen Betriebssystemen? Debians Moralvorstellungen! 
Es ist der [Debians Gesellschaftsvertrag (Debian Social Contract)](https://www.debian.org/social_contract).

Es gibt noch weitere Gründe warum Debian bei Anwendern sehr beliebt ist. Es wird als sehr stabil und
zuverlässig angesehen und ist sehr universell einsetzbar.

 * [Gründe für den Einsatz von Debian](https://www.debian.org/intro/why_debian)

### Lust auf mehr bekommen?

Die aktuelle Version von Debian GNU/Linux ist Debian 12 (Codename "Bookworm").
Es gibt mehrere Möglichkeiten das Betriebssystem zu installieren/auszuprobieren. 
Was wir später im Artikel noch mal ansprechen werden.

Das Debian Projekt hat sehr viel Dokumentation.

 * [Debian GNU/Linux – Installationsanleitung](https://www.debian.org/releases/stable/installmanual)
 * [Hinweise zur Debian-Veröffentlichung Version 12 (Bookworm)](https://www.debian.org/releases/stable/releasenotes)
 * [Die Debian GNU/Linux-FAQ](https://www.debian.org/doc/manuals/debian-faq/index.de.html)
 * [Debian-Referenz](https://www.debian.org/doc/manuals/debian-reference/)
 * [Das Debian Administrationshandbuch](https://www.debian.org/doc/manuals/debian-handbook/index.de.html)
 * [Handbücher (Manpages)](https://manpages.debian.org/)
 * [Debian Wiki](https://wiki.debian.org/de/FrontPage)

Was in Debian "drin" ist, findet man auf der Seite
[Paketsuche](https://www.debian.org/distrib/packages). In Debian Stable sind
mehr als 59000 Pakete verfügbar. Möchte man mehr zu einer Software in Debian erfahren,
findet man alle wichtigen Informationen auf diesen Seiten.
[Hier](https://packages.debian.org/bookworm/dino-im) ein Beispiel zur Paketseite
vom Chat Programm "dino".

### Von Windows zu Debian GNU/Linux

Unserer Erfahrung nach ist der Schritt von Windows zu GNU/Linux meist ein
Prozess welcher einige Zeit in Anspruch nehmen kann. Welchen Weg man wählt hängt von den
persönlichen Bedürfnissen sowie Anforderungen ab. 

Eine gute Voraussetzung, den Prozess von Windows zu Linux zu starten, ist wenn
man einen ausgemusterten Computer hat oder das System in einer virtuellen
Maschine installieren kann. So kann man das System langsam kennenlernen und nach
und nach auf seine Bedürfnisse anpassen.

Es gibt auch die Möglichkeit Live-Images zu nutzen. Ein Live-System kann direkt
von USB/CD/DVD gestartet werden und es muss nichts installiert werden.
Debian bietet [mehrere Möglichkeiten](https://www.debian.org/distrib/) für Abbilder an.

Hilfreich in verschiedenster Hinsicht, kann das aufsuchen einer Linux User Group
sein. Mit etwas Glück findet man eine Gruppe in der Nähe. Es
gibt einige hilfsbereite Menschen, die Fragen beantworten und beim Lösen
von Problemen unterstützen können. Es gibt auch innerhalb des Debian Projekts
verschiedene Online Support Gruppen (Mailinglisten, IRC Kanäle, XMPP
Gruppenchats, Foren).

### Danke, Danke, Danke!

An dieser Stelle möchten wir den Menschen danken, welche am Debian Projekt
und an freier Software mitarbeiten. Der Linux Kernel, die GNU Software, das Debian
Projekt und viele Software Entwickler welche ihre Software unter einer freien
Lizenz anbieten, haben es ermöglicht, dass wir ein freies Betriebssystem nutzen
können. Frei nicht im Sinne von kostenlos, sondern von "freiheitlich".
