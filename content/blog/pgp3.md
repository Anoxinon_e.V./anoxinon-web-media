---
title: OpenPGP - Teil 3 - E-Mails verschlüsseln und signieren
date: "2022-02-01T19:18:00+00:00"
tags:
- Fortgeschritten
categories:
- Sicherheit
- Datenschutz
- Freie Software
banner: "/img/thumbnail/openpgp-final.png"
series: pgp
description: "E-Mails sind in der Regel unverschlüsselt, jede:r der Zugriff auf eine E-Mail hat kann diese lesen und verändern - den E-Mail Anbietenden eingeschlossen. Wie man dieses Manko verhindert und mithilfe von OpenPGP die Sicherheit von E-Mails erhöhen kann, zeigen wir in diesem Artikel."
---

**Inhaltsverzeichnis:**

1. [Einleitung](#einleitung)
2. [Wozu OpenPGP bei E-Mails?](#wozu-openpgp-bei-e-mails)
3. [Die Möglichkeiten, OpenPGP in E-Mails zu verwenden](#die-möglichkeiten-openpgp-in-e-mails-zu-verwenden)
4. [OpenPGP mit Claws-Mail](#openpgp-mit-claws-mail)
5. [Fazit](#fazit)
6. [Ausblick](#ausblick)
7. [Literatur und Quellen](#literatur-und-quellen)


## Einleitung

E-Mails sind i.d.R unverschlüsselt. Jede:r der Zugriff auf eine E-Mail hat, kann den Inhalt lesen und verändern. E-Mail-Provider können den Inhalt von E-Mail analysieren und auswerten. Dies kann anonyme aber auch pseudonymisiert erfolgen. Auch E-Mails mit gefälschtem Absender oder gefälschter Absenderin können ein Problem sein. Für die sichere Kommunikation ist das keine guten Voraussetzung. Abhilfe kann OpenPGP schaffen, das wir in Grundzügen schon in den ersten beiden Teilen dieser Artikelserie vorgestellt haben ([OpenPGP - Teil 1 - Einleitung](https://anoxinon.media/blog/pgp1/) und [OpenPGP - Teil 2 - Der öffentliche Schlüssel](https://anoxinon.media/blog/pgp2/)).

---
**Teil einer Artikelserie:**

{{< series-parts >}}

---

## Wozu OpenPGP bei E-Mails?

Mit OpenPGP lässt sich die E-Mail-Kommunikation sicherer gestalten:

1. **Mit OpenPGP lassen sich E-Mails verschlüsseln.** Das dient dem Schutzziel der Vertraulichkeit, damit die E-Mail nur von dem bestimmten Empfänger oder der Empfängerin gelesen werden kann.
2. **Mit OpenPGP lassen sich E-Mails signieren.** Dies dient dem Schutzziel der Integrität, damit die Senderin oder der Sender einer Nachricht fälschungssicher festgestellt werden kann.

## Die Möglichkeiten, OpenPGP in E-Mails zu verwenden

Viele E-Mail-Programme unterstützen auf dem einen oder dem anderen Weg OpenPGP:

1. Manche E-Mail-Programme bringen OpenPGP-Unterstützung von Haus aus mit
2. Bei anderen wiederum lässt es sich mittels Erweiterung oder Plugin nachrüsten

Darüberhinaus gibt es noch eine weitere wichtige Unterscheidung, die beachtet werden muss: Die Schlüsselverwaltung. Um OpenPGP zu nutzen, benötigt es neben dem eigenen Schlüsselpaar die öffentlichen Schlüssel der Kommunikationpartner:innen. Diese wollen von dir verwaltet werden:

1. Manche E-Mail-Programme integrieren sich in eine zentrale Schlüsselverwaltung auf deinem PC, wie bspw. das in den vorherigen Teilen verwendete GPG oder GPA.

2. Andere E-Mailer wiederum integrieren eine seperate Schlüsselverwaltung.

Oft unterscheidet sich die Verwendung der verschiedenen Optionen nur bei der Einrichtung. Ist es einmal eingerichtet, kann OpenPGP verwendet werden.

## OpenPGP mit Claws-Mail

Folgend zeigen wir die Einrichtung von OpenPGP mit Claws Mail. Claws Mail greift auf die OpenPGP-Schlüsselverwaltung von deinem System zurück, du musst diese also schon eingerichtet haben (das haben wir in [OpenPGP Teil 1](/blog/pgp1/) angerissen, dort findet sich auch [ein Link zu einer Beschreibung dafür im Wiki](/wiki/programme/sicherheit/gpa/)).

---

**Hinweis:** Falls du noch kein Claws Mail verwendest, findest du [in unserem Wiki eine Beschreibung](/wiki/programme/kommunikation/claws-mail/) wie du es sowohl für _Linux_ als auch _Windows_ einrichten kannst.

---

### OpenPGP aktivieren

Die Verwendung von OpenPGP ist in claws-mail mit den Plugins PGP/Core, PGP/inline und PGP/MIME möglich. Diese müssen dafür aktiviert sein. In den Konto-Optionen lassen sich nun Einstellungen im Abschnitt `Datenschutz` und im Abschnitt `Plugins/PGP` vornehmen.

![claws-mail-01.png](/img/pgp/claws-mail-01.png)

### OpenPGP Standardeinstellungen treffen

Nun wo uns OpenPGP in Claws Mail zur Verfügung steht, wollen wir das Standardverhalten des E-Mail Clients konfigurieren. Wir verwenden in unserem Beispiel PGP MIME und wählen alle Optionen aus.

* Nachrichten immer signieren
* Nachrichten immer verschlüsseln
* Nachrichten stets signieren, wenn eine signierte Nachricht beantwortet wird
* Nachrichten stets verschlüsseln, wenn eine verschlüsselte Nachricht beantwortet wird
* Nachrichten zusätzlich zum fremden mit eigenem Schlüssel verschlüsseln

![claws-mail-02.png](/img/pgp/claws-mail-02.png)Im Abschnitt Plugin/GPG wählen wir die Option "Schlüssel nach Ihrer E-Mail-Adresse wählen". Abhängig von der verwendeten Absenderadresse wird der jeweilige OpenPGP Key verwendet.

![claws-mail-03.png](/img/pgp/claws-mail-03.png)

### Verschlüsselte E-Mails schreiben

Die Grundkonfiguration ist jetzt eingerichtet und wir können mit dem Schreiben von verschlüsselten E-Mail beginnen.

Bei dem Verfassen von E-Mails ändert sich für die Benutzenden nichts. Der Benutzende wählt die E-Mail-Adresse des Empfangenden aus und gibt ein Betreff ein. Dann kann er/sie mit dem Schreiben der E-Mail wie gewohnt beginnen.  Die Einstellungen lassen sich im Menü "Optionen" anpassen.

![claws-mail-04.png](/img/pgp/claws-mail-04.png)

---
**Beachte:** Nicht alle Daten innerhalb einer E-Mail werden verschlüsselt. Informationen im "Header" einer E-Mail sind nicht verschlüsselt. Dazu zählen unter anderem Absender, Empfänger, Betreff und Datum. Man sollte also drauf achten, dass bei einer verschlüsselten E-Mail keine sensiblen Informationen im Betreff sind.

---

![claws-mail-05.png](/img/pgp/claws-mail-05.png)

Konnte kein Schlüssel auf deinem Schlüsselbund dem Empfangenden eindeutig zugeordnet werden, muss der Benutzende den öffentlichen Schlüssel des Gegenübers auswählen. 

![claws-mail-06.png](/img/pgp/claws-mail-06.png)

### Verschlüsselte E-Mails empfangen

Bei Empfang einer verschlüsselten Nachricht, wird ein Schlüssel in der Liste der E-Mails angezeigt. Diese E-Mail kann nur gelesen werden, wenn der Benutzende Zugriff auf den privaten Schlüsselt hat, der zu dem öffentlichen Schlüssel gehört, mit dem die E-Mail verschlüsselt wurde.

![claws-mail-07.png](/img/pgp/claws-mail-07.png)Im unteren Bereich findet man ein Schloss mit der Beschriftung "Gültige Signatur von ...". Dies ist die Prüfung der Signatur des Absenders. Es gibt an, dass die E-Mail von der Person unterschrieben wurde.

## Fazit
Auch wenn OpenPGP nicht ganz intuitiv ist, sind die Vorteile nicht von der Hand zu weisen. Es lohnt sich also die Einarbeitungszeit zu Anfang, denn einmal eingerichtet und angewöhnt, wirst du merken, dass es doch gut und gewohnt von der Hand geht. Von dem her bleibt dir nur anzuraten, diesen Artikel zu Herzen zu nehmen und es einmal auszuprobieren.

## Ausblick
Nun sind wir in der Artikelserie schon in der praktischen Anwendung von OpenPGP angekommen. Das werden wir in zukunftigen Artikeln ausführen, unter anderem mit folgenden Themen:

- Schlüssel mit OpenPGP verwalten
- LibreOffice Dokumente mit OpenPGP verschlüsseln
- Und noch weitere...
