---
title: OpenPGP - Teil 5 - Schlüsselverwaltung mit GPA
date: "2022-04-01T19:30:00+00:00"
tags:
- Fortgeschritten
categories:
- Sicherheit
- Datenschutz
- Freie Software
banner: "/img/thumbnail/openpgp-final.png"
series: pgp
description: >
    Wichtig für einen produktiven Einsatz von OpenPGP ist  eine Verwaltung der entsprechenden OpenPGP-Schlüssel(paare). Dies erledigt man in aller Regel mit einer Schlüsselverwaltung, also einem Programm, das genau für diesen Anwedungszweck gedacht ist und eine Vielzahl von Funktionen rund um OpenPGP-Schlüssel vereint.
---

**Inhaltsverzeichnis:**

1. [Einleitung](#einleitung)
2. [Was ist eine OpenPGP-Schlüsselverwaltung?](#was-ist-eine-openpgp-schlüsselverwaltung)
3. [Was ist ein Schlüsselbund?](#was-ist-ein-schlüsselbund)
4. [Arten von Schlüsselverwaltungen](#arten-von-schlüsselverwaltungen)
5. [GPA - Der GNU Privacy Assistant](#gpa-der-gnu-privacy-assistant)
    - [Was ist GPA?](#was-ist-gpa)
    - [Einrichtung](#einrichtung)
    - [Schlüsselpaar erstellen](#schlüsselpaar-erstellen)
    - [Privaten Schlüssel bearbeiten](#privaten-schlüssel-bearbeiten)
    - [Öffentliche Schlüssel anderer bearbeiten](#öffentliche-schlüssel-anderer-bearbeiten)
    - [Dateien verschlüsseln](#dateien-verschlüsseln)
6. [Fazit](#fazit)

## Einleitung

In den vergangenen Artikeln haben wir die theoretischen Grundlagen von OpenPGP behandelt und sind auf einige praktische Beispiele eingegangen.
Essentiell für einen produktiven Einsatz von OpenPGP ist jedoch eine Verwaltung der entsprechenden OpenPGP-Schlüssel(paare). Dies erledigt man in aller Regel mit einer Schlüsselverwaltung, also einem Programm, das genau für diesen Anwedungszweck gedacht ist und eine Vielzahl von Funktionen rund um OpenPGP-Schlüssel vereint.

Wenn du bisher nicht alle Artikel unserer OpenPGP-Artikelserie gelesen hast, dann ist dir das nur ans Herz zu legen. Du benötigst mindestens die Informationen aus [OpenPGP - Teil 1 - Einleitung](/blog/pgp1/) und [OpenPGP - Teil 2 - Der öffentliche Schlüssel](/blog/pgp2/) in denen wir die absoluten Basics zu Schlüsseln, deren Funktion und Aufbau erläutern. 

Viel Spaß beim Lesen!

---
**Teil einer Artikelserie:**

{{< series-parts >}}

---

## Was ist eine OpenPGP-Schlüsselverwaltung?

Eine OpenPGP-Schlüsselverwaltung ist ein wichtiger Bestandteil, um OpenPGP produktiv einsetzen zu können. Die Kernaufgabe dieser ist, wie der Name schon andeutet, die Schlüsselverwaltung. Dazu zählen Aufgaben wie:


- Das Erstellen von Schlüsselpaaren
- Das Anpassen von Schlüsseleigenschaften
    - Hinzufügen von Identitäten
    - Ändern des Ablaufdatums
    - ...
- Das Verteilen von öffentlichen Schlüsseln
    - Als Datei
    - Hochladen zum Web of Trust
    - ...
- Das Exportieren/Sichern von eigenen Schlüsselpaaren
- Das Importieren/Hinzufügen von eigenen Schlüsselpaaren
- Das Hinzufügen von fremden, öffentlichen Schlüsseln
    - Aus Dateien
    - Herunterladen aus dem Web of Trust
    - Herunterladen aus dem Web Key Directory
    - ...
- Und so weiter...

## Was ist ein Schlüsselbund?

Ein Schlüsselbund bezeichnet die Gesamtheit aller OpenPGP-Schlüssel die man auf dem eigenen Rechner gespeichert hat und mit denen man interagiert. Der Schlüsselbund wird von der Schlüsselverwaltung verwaltet.

## Arten von Schlüsselverwaltungen

Es gibt grundlegend zwei Arten von Schlüsselverwaltungen:

- **Zentrale/Systemweite Schlüsselverwaltung:** Ein dediziertes Programm für die OpenPGP-Schlüsselverwaltung, auf das andere Programme, die OpenPGP verwenden, zugreifen können
- **Programmspezifische Schlüsselverwaltung:** Eine im jeweiligen OpenPGP-nutzenden Anwendungsprogramm integrierte Schlüsselverwaltung. 

Der Vorteil von ersterer liegt auf der Hand: Der Schlüsselbund wird von einer Instanz verwaltet. Im Gegensatz dazu steht die zweite Option, für die man jeden Schlüssel für jedes etwaige Anwendungsprogramm separat verwalten muss und so doppelt- oder dreifache Arbeit hat.

Jedoch hat auch die zweite Variante einen Vorteil: Sie mindert die Einstiegshürde. Programme können OpenPGP verwenden, ohne dass der Nutzende sich je damit auseinandergesetzt haben muss und ohne dass zuvor OpenPGP auf dem Rechner eingerichtet sein muss.

## GPA - Der GNU Privacy Assistant

### Was ist GPA?

Der **GNU Privacy Assisant (kurz GPA)** ist die grafischen Schlüsselverwaltung vom GNU-Projekt und baut auf der OpenPGP-Implementierung **GnuPG** auf. Sie steht sowohl für diverse Linux-Distributionen, als auch Apple MacOS und Microsoft Windows zur Verfügung. GPA fällt dabei unter die Art der zentralen Schlüsselverwaltung, das heißt sie ist ein dediziertes Programm und dient (wie übrigens GnuPG auch) als Basis/Systemkomponente für andere Programme, die OpenPGP verwenden wollen.

Nachfolgend möchten wir die Einrichtung und einige grundlegende Funktionen vorstellen.

### Einrichtung

#### Linux

GPA ist weit verbreitet und daher in den meisten Linux-Distributionen von Haus aus enthalten, bei allen relevanten Distros unter dem Paketnamen `gpa`. Erfahrungsgemäß machen allerdings die Linux-Appstores Probleme, wenn der Paketname nur drei Buchstaben hat und auch anhand der Beschreibung findet man GPA nicht immer. Daher empfiehlt sich eine Installation über die Kommandozeile. Für die bekanntesten Paketmanager findest du [eine Beschreibung bei uns im Wiki, wie du das bewerkstelligen kannst](/wiki/betriebssysteme/linux/paketmanager/).

1. Das Paket `gpa` installieren

#### Windows

Unter Windows lässt sich GPA am einfachsten via GPG4Win installieren. GPG4Win ist eine Sammlung von Windows-Programmen für OpenPGP. 

1. Herunterladen auf [gpg4win.org](https://gpg4win.org/get-gpg4win.html). Die Entwicklenden bitten vor dem Download um eine Spende. Wenn du nichts Spenden möchtest, kannst du einfach $0 anwähln.
2. Den Installer starten, Sprache auswählen und nach eigenen Vorlieben durchklicken
3. Beim Schritt "Komponenten auswählen" darauf achten, **GPA** anzuhaken

{{< bildstrecke gpa_installation_windows >}}

### Schlüsselpaar erstellen

Beim ersten Start bekommt man die Möglichkeit, interaktiv ein neues Schlüsselpaar zu erstellen.

{{< bildstrecke gpa_einrichten >}}

### Privaten Schlüssel bearbeiten

#### Ablaufdatum und Passwort ändern

Das Ablaufdatum und das Passwort eines geheimen Schlüssels lassen sich komfortabel via GPA-Oberfläche bearbeiten. Dafür wählt man das betreffende Schlüsselpaar via Rechtsklick an und klickt _geheimen Schlüssel bearbeiten_.

![](/img/pgp/private_key_edit/01_gpa_private_key_edit_e.png)

Im folgenden Fenster kann man unter _(1)_ das Passwort anpassen, mit dem der geheime Schlüssel geschützt wird und unter _(2)_ ein Ablaufdatum festsetzen oder ändern. 

![](/img/pgp/private_key_edit/02_gpa_private_key_edit_e.png)


### Öffentliche Schlüssel Anderer bearbeiten

Neben dem eigenen Schlüsselpaar kann man auch andere öffentliche Schlüssel mit GPA bearbeiten. Wieder gelangt man via Rechtsklick auf den entsprechenden Schlüssel zu den spannenden Menüpunkten. Interessant sind vor allem die Punkte _(1) Schlüssel signieren_ und _(2) Schlüssel-Vertrauen einstellen_.


![](/img/pgp/pubkey_edit/01_gpa_pubkey_edit_e.png)

**(1) Schlüssel signieren:** Mit dieser Option kann man den öffentlichen Schlüssel anderer mit dem eigenen privaten Schlüssel signieren. Damit bestätigt man die Echtheit dieses öffentlichen Schlüssels und gibt an, dass dieser von der Person stammt, die er angibt. Es ist eine digitale, kryptografische Unterschrift des öffentlichen Schlüssels. Es ist die Basis für das sogenannte "Web of Trust", was wir uns in einem kommenden Beitrag genauer ansehen werden.

![](/img/pgp/pubkey_edit/03_gpa_pubkey_edit.png)

**(2) Schlüssel-Vertrauen einstellen** präsentiert das folgende Fenster und erlaubt das Festlegen des Schlüsselvertrauens. Das spielt bei der Bewertung von Schlüsselsignaturen eine Rolle, um die Gültigkeit eines öffentlichen Schlüssels zu bewerten. Wenn beispielsweise das Vertrauen "Vollständig" gewählt wurde, vertrauen sie jedem (anderen) öffentlichen Schlüssel, der eine (gültige) Signatur von dem als vetrauenswürdig markierten Schlüssel trägt.

![](/img/pgp/pubkey_edit/02_gpa_pubkey_edit.png)


### Dateien verschlüsseln

Mit GPA lassen sich nicht nur Schlüssel verwalten, sondern auch direkt Dateien verschlüsseln. Dazu muss man in die Übersicht _Dateien_ wechseln:

![](/img/pgp/file_enc/01_gpa_file_enc_e.png)

Dort findet man eine Dateiübersicht/-verwaltung. Noch ist sie leer. Mit Klick auf _Öffnen_ lassen sich Dateien in die Übersicht hinzufügen.

![](/img/pgp/file_enc/02_gpa_file_enc_e.png)

Hat man Dateien in der Dateiverwaltung hinzugefügt, so kann man sie verschlüsseln, indem man - wie soll es anders sein - auf _Verschlüsseln_ klickt.

![](/img/pgp/file_enc/03_gpa_file_enc_e.png)

Um die Verschlüsselung durchzuführen, muss GPA wissen, für wen es die Datei verschlüsseln soll. Dazu muss man den passenden öffentlichen Schlüssel der Empfängerin oder des Empfängers auswählen. Nur diejenige Person kann die Datei dann noch lesen. Möchte man selbst auch die verschlüsselte Datei wieder öffnen können, muss man die Datei logischerweise also auch an den eigenen öffentlichen Schlüssel verschlüsseln. Wahlweise kann man die Datei zusätzlich signieren, dazu hakt man den gleichnamigen Haken im GPA-Fenster an.

![](/img/pgp/file_enc/04_gpa_file_enc.png)

Nach Bestätigung wird die verschlüsselte Datei standardmäßig unter gleichem Dateinamen mit _.gpa_ als Suffix abgespeichert. Diese kann man gefahrlos weitergeben, der Inhalt kann nicht gelesen werden, ohne dass die Datei zuvor entschlüsselt wurde.

![](/img/pgp/file_enc/05_gpa_file_enc_e.png)

Für die Entschlüsselung geht man analog vor, hierzu wählt man statt _Verschlüsseln_ in der Dateiverwaltung _Entschlüsseln_. Dies klappt nur, wenn man über den privaten Schlüssel zum zugehörigen öffentlichen Schlüssel verfügt, für den die Datei verschlüsselt wurde. Gegebenenfalls muss man ebenso das Passwort des privaten Schlüssels eingeben.

Wer sich speziell für die Verschlüsselung und Entschlüsselung von Dokumenten interessiert, dem/der sei an der Stelle der vierte Beitrag unserer OpenPGP Artikelreihe an das Herz gelegt -> ["OpenPGP - Teil 4 - Dokumente verschlüsseln und signieren"](/blog/pgp4/).

## Fazit

Für alle, die eher auf grafischen Oberflächen unterwegs sind, ist GPA eine sowohl leichtgewichtige als auch mächtige Lösung zur Verwaltung des eigenen Schlüsselbunds. Auch Einsteiger:innen in Sachen OpenPGP kann es GPA durch die grafische Übersicht einiger gängiger Funktionen und Arbeitsweisen von OpenPGP eine Unterstützung sein. Alle Interessenten haben nun mit diesem Beitrag einen Leitfaden für den Einstieg an der Hand und einem Ausprobieren steht nichts mehr im Wege.
