---
title: XMPP - Teil 1 - Messaging mal anders 
date: "2022-09-01T07:30:00+02:00"
tags:
- Fortgeschritten
categories:
- Freie Software
banner: "/img/xmpp/xmpp-series-thumbnail.png"
description: Getting started mit XMPP, Messaging mal anders
series: xmpp
---

**Inhaltsverzeichnis:**

1. [Einleitung](#einleitung)
2. [Der Ablauf](#der-ablauf)
3. [Anbieter aussuchen](#anbieter-aussuchen)
4. [Account anlegen](#account-anlegen)
5. [Anwendung installieren](#anwendung-installieren)
6. [Chatten](#chatten)

### Einleitung

Instant-Messaging-Lösungen gibt es mittlerweile wie Sand am Meer. Oft wird mit
Sicherheit und Datenschutz geworben, doch trotzdem gibt es zwischen den
verschiedenen Lösungen teils gravierende Unterschiede. Im Folgenden soll es um 
XMPP gehen - eine Alternative, die sich von der Masse abhebt.

Es hört sich vielleicht erst mal kompliziert an, das ist es aber eigentlich gar
nicht. Wir werden in der Einleitung ganz kurz drauf eingehen was XMPP ist, aber
dann auch ganz schnell die ersten Schritte beschreiben, wie man mit XMPP
loschatten kann.

XMPP steht für E**x**tensible **M**essaging and **P**resence **P**rotocol, was auf
deutsch in etwa heißt *erweiterbares Nachrichten- und Anwesenheitsprotokoll*. Es
handelt sich hierbei um ein offenes und standardisiertes Protokoll, welches man
für *Instant Messaging* - also "chatten" verwenden kann.

Ein Protokoll ist eine Definition von syntaktische und semantische Regeln, wie
die verschiedenen Anwendungen miteinander kommunizieren. Nur wenn das
Protokoll offen ist, können andere Anwendungsentwickler ebenfalls Programme
schreiben, die dieses Protokoll verwenden. Dies bedeutet auch, dass es nicht 
*die* eine Anwendung für XMPP gibt. Tatsächlich gibt es für XMPP eine Vielzahl
an Anwendungen, die größtenteils miteinander kompatibel sind.

So lassen sich anbieterunabhängige Lösungen umsetzen. Dies kennen die meisten
von E-Mails. Auch hier gibt es viele verschiedene Anbieter und auch viele
verschiedene Anwendungen. Dennoch können die Nutzer E-Mails unabhängig
von Anbieter und Anwendung zur Kommunikation verwenden.

### Der Ablauf

Zu Beginn brauchen wir einen Anbieter, welcher uns einen XMPP Dienst zur Verfügung
stellt. Wer noch keinen Anbieter hat, kann sich vorher Gedanken machen, welche
Kriterien für ihn wichtig sind und sich dann einen Anbieter aussuchen, welcher
die gewünschten Kriterien erfüllt.

Nach der Auswahl des Anbieters, erstellt man sich einen XMPP Account. In den
meisten Fällen sucht man sich einen Namen aus und wählt ein Passwort für den
Account. Nach der Anmeldung hat man ein XMPP Account, welche von der Form
vergleichbar mit einer E-Mail Adresse ist (name@domain.tld).

Im nächsten Schritt entscheidet man sich für eine Anwendung, dem XMPP Client.
Diese wird installiert und man trägt seine XMPP-Adresse und sein Passwort für
den XMPP Account ein.

Fertig! Willkommen bei XMPP!

1. Anbieter aussuchen
2. Account anlegen
3. Anwendung bzw. App installieren
4. Account in der Anwendung hinzufügen

![Ein Screenshot vom gajim - Links das Addressbuch und rechts ein Gruppenchat](/img/xmpp/xmpp-1-gajim.png)

### Anbieter aussuchen

Gerne laden wir euch ein, unseren [XMPP Dienst](https://anoxinon.de/dienste/anoxinonmessenger/)
zu nutzen, wenn ihr dies möchtet. Eine Liste von Anbietern findet man hier:

* [Liste von XMPP-Anbieter:innen auf anoxinon.media](https://anoxinon.media/wiki/dienste/xmpp/)
* [Empfehlenswerte Server auf Freie Messenger](https://www.freie-messenger.de/sys_xmpp/server/#empfehlenswerte-server)
* [Provider Recommendations auf xmpp.org](https://wiki.xmpp.org/web/Provider_Recommendations)

Die folgenden Kriterien sollte man bei der Wahl des Anbieters beachten:

* Wer betreibt den Dienst
* Standort des Servers
* Dauer der Speicherung von Nachrichten
* Wie aktuell ist die Software
* Welche Erweiterungen werden vom Server unterstützt

Die wesentlichen Vorteile sind, dass der Dienst nicht nur von einem Anbieter
angeboten wird, sondern mehrere Anbieter den Dienst bereitstellen können. Der
Benutzer kann so frei wählen, wo er den Dienst nutzen möchte oder ggf. auch
selber einen eigenen Dienst betreiben. Außerdem kann ein Benutzer mehrere
Accounts haben, welche auch bei unterschiedlichen Anbietern sein können.

### Account anlegen

Die meisten öffentlichen Anbieter haben eine Webseite, auf welcher der Benutzer
seinen Account erstellen kann. Der Benutzer wählt einen Benutzernamen, dies kann
ein fiktives Wort, eine (zufällige) Zeichenfolge oder sein richtiger Name sein. 
Zudem muss ein Passwort gewählt werden, welches man sich gut merken sollte. 
Erstellt man beispielsweise einen Account `MaxMustermann` beim Anbieter `domain.tld`,
so ist die XMPP-Adresse `MaxMustermann@domain.tld`.

Einige Server bieten auch eine *in-band registration* (IBR). Wenn diese Funktion
angeboten wird, kann man seinen Account auch direkt innerhalb einer Anwendung
erzeugen. Allerdings bieten nicht alle Anbieter und Anwendungen diese Funktion an.

### Anwendung installieren

Jetzt ist es Zeit sich eine Client Anwendung für XMPP zu installieren. Es gibt
viele verschiedene XMPP Client Anwendungen. Abhängig von persönlichen
Bedürfnissen und Geschmack, kann man sich ein Client aussuchen.

* [Conversations](https://conversations.im) ist eine Client für Android. 
  Er kostet im PlayStore nicht mal 4 Euro. Im F-Droid Store gibt es die App kostenlos.
* [blabber.im](https://blabber.im/) ist ein Conversations Fork. Die App ist
  kostenlos im F-Droid.
* [Quicksy](https://quicksy.im/) ist ein Ableger von Conversations. Während man
  bei Conversations XMPP-Adressen hinzufügt, funktioniert Quicksy via
  Handynummer. So lassen sich Kontakte aus dem Adressbuch leicht finden.
* [gajim](https://gajim.org/) ein Client für GNU/Linux und Windows.
* [Dino](https://dino.im/) ein Client für GNU/Linux.
* [Profanity](https://profanity-im.github.io) ein Client für die GNU/Linux
  Console.
* [conversejs](https://conversejs.org/) ist ein XMPP Web-Client. Auch wir bieten
  diesen Web-Client auf https://webchat.anoxinon.me/ an.
* Weitere XMPP Client Anwendungen findet man auf
  [xmpp.org](https://xmpp.org/software/clients.html) oder [jabber.de](https://www.jabber.de/clients/)

![Ein Screenshot vom dino - Links das Addressbuch und rechts ein Gruppenchat](/img/xmpp/xmpp-1-dino.png)

### Chatten

Nachdem man seinen XMPP-Account (XMPP-Adresse und Passwort) in der Anwendung
eingerichtet hat, kann man mit dem Chatten beginnen. Man kann andere XMPP-Benutzer
in seiner Kontaktliste eintragen, den Online-Status von Kontakten sehen und
innerhalb privater und öffentlicher Gruppen chatten. Natürlich sind auch 1:1
Chats möglich.

Die meisten Anwendungen funktionieren mit der XMPP-Adresse. Dies bedeutet, dass
man für den Dienst weder eine E-Mail-Adresse noch eine Telefonnummer benötigt.
Aus diesem Grund hat XMPP eine eigene Kontaktliste (in der Spezifikation "roster"
genant). Man fügt seine Kontakte über die jeweilige XMPP-Adresse des Bekannten
hinzu.

### Vor- und Nachteile

Die Verwendung von XMPP hat wesentlichen Vorteile gegenüber manch anderen Messenger.
Eine Medaille hat allerdings immer zwei Seiten. Denn was für den einen
vielleicht ein Vorteil ist, kann aus einem anderen Gesichtspunkt ein Nachteil
sein. Hier ein paar Punkte, die wir für euch zusammengestellt haben.

* Es gibt viele XMPP Client Anwendungen, welche als Freie Software angeboten
  werden. Der Benutzer ist nicht gezwungen eine konkrete Anwendung zu verwenden.
* Es gibt verschiedene XMPP Server Software, welche als Freie Software angeboten werden.
* Bei einer Vielzahl öffentlicher Anbieter sind für die Registrierung keine weiteren
  Daten notwendig (bswp. E-Mail-Adresse, Telefonnummer, ...).
* Viele Client Anwendungen bieten dem Nutzer die Möglichkeit, mehrere Accounts
  zu verwenden. So ist beispielsweise eine Trennung von privater und beruflicher
  Kommunikation möglich.
* Die Accounts lassen sich unabhängig voneinander "online" und "offline"
  schalten. Auch der Status ist unabhängig wählbar. (Diese Funktion ist jedoch clientabhängig)
* XMPP lässt sich auch ohne Smartphone/Telefon verwenden, wenn eine Person
  kein Smartphone besitzt oder das Chatten nur auf dem PC wünscht.
* Wenn jemand kein eigenen Computer hat, kann er einen Web-Client nutzen.

### Fragen?

Versuche es doch einfach mal. Du kannst auf unserem Server einen
[Account anlegen](https://anoxinon.de/dienste/anoxinonmessenger/#registrieren)
und es einfach im [Webchat](https://webchat.anoxinon.me/) ausprobieren oder dir
direkt eine Anwendung installieren. Wenn du noch weiter Hilfe benötigst, kannst du gerne unserem öffentlichen Anoxinon-Gruppenchat direkt auf XMPP beitreten: [anoxinon@conference.anoxinon.me](xmpp://anoxinon@conference.anoxinon.me?join).
