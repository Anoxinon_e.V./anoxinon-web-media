---
title: OpenPGP - Teil 6 - die Vertrauenswürdigkeit der öffentlichen Schlüssel
date: "2022-05-01T16:30:00+00:00"
tags:
- Fortgeschritten
categories:
- Sicherheit
- Datenschutz
- Freie Software
banner: "/img/thumbnail/openpgp-final.png"
series: pgp
description: >
    In den vergangenen Artikeln haben wir sowohl die theoretischen Grundlagen von OpenPGP behandelt sowie einige praktische Anwendung. Was wir dort gelernt haben, steht und fällt aber mit einer zentralen Fragen: Ist der öffentliche Schlüssel wirklich von der Person, von der wir das glauben? Die beste Signatur oder die beste Verschlüsselung ist nutzlos, wenn der Schlüssel, den wir verwenden, von einer Angreiferin oder einem Angreifer stammt. Beispielsweise hindert uns niemand daran, beliebig Schlüssel für beliebige Identitäten (bspw. E-Mail-Adressen) zu erstellen.
---

**Inhaltsverzeichnis:**

1. [Einleitung](#1-einleitung)
2. [Schlüsselbeschaffung und Lieferketten](#2-schlüsselbeschaffung-und-lieferketten)
3. [Schlüsselüberprüfung mit Fingerabdrücken](#3-schlüsselüberprüfung-mit-fingerabdrücken)
4. [Vertrauen bis Vertrauen⁵](#4-vertrauen-bis-vertrauen⁵)
5. [Schlüssel signieren](#5-schlüssel-signieren)
6. [TOFU](#6-tofu)
7. [Fazit](#7-fazit)

## 1. Einleitung

In den vergangenen Artikeln haben wir sowohl die theoretischen Grundlagen von OpenPGP behandelt sowie einige praktische Anwendung. Was wir dort gelernt haben, steht und fällt aber mit einer zentralen Fragen: Ist der öffentliche Schlüssel wirklich von der Person, von der wir das glauben? 

Die beste Signatur oder die beste Verschlüsselung ist nutzlos, wenn der Schlüssel, den wir verwenden, von einer Angreiferin oder einem Angreifer stammt. Beispielsweise hindert uns niemand daran, beliebig Schlüssel für beliebige Identitäten (bspw. E-Mail-Adressen) zu erstellen. 

{{< series-parts >}}

## 2. Schlüsselbeschaffung und Lieferketten

Im [Teil 2](/blog/pgp2) sind wir kurz auf die Verteilung der Schlüssel an andere Benutzer:innen eingegangen.
Bei der Nutzung eines Speichermediums wie einem USB-Sticks, der persönlich übergeben wird, können wir uns sehr
sicher sein, dass sich darauf der echte öffentliche Schlüssel befindet.
Das genaue Gegenteil sind öffentliche Keyserver, auf die jeder Schlüssel hochladen kann.
Das ist [scheinbar](https://keyserver.ubuntu.com/pks/lookup?search=a.merkel%40bundestag.de&fingerprint=on&op=index) kein theoretisches Problem.
Selbst wenn der Keyserver nur einen Schlüssel anbietet weiß man nicht, ob das ein echter oder ein falscher Schlüssel ist.

Dazwischen liegen das Web Key Directory und das Versenden des Schlüssels per E-Mail als Antwort auf eine an diesen Benutzer gesendete E-Mail.
Beide Methoden stellen sicher, dass der Schlüssel vom Benutzer kommt
oder von jemanden mit Kontrolle über die beteiligte E-Mail-Infrastruktur platziert wurden, was den Kreis der möglichen Angreifer erheblich reduziert.
Wenn man nicht von besonderem Interesse ist und auch keine Geschäftsgeheimnisse austauschen möchte, sondern nur belanglose Alltagskommunikation durchführt,
dann kann man bei diesen beiden Methoden für sich entscheiden, dass der Schlüssel mit ausreichend hoher Wahrscheinlichkeit echt ist.
(Und falls man von besonderem Interesse ist oder Geschäftsgeheimnisse austauschen möchte, sollte man über mehr Sicherheitsmaßnahmen als die
Verschlüsselung der Kommunikation nachdenken und auch an die Sicherheit beim Kommunikationspartner denken.)
Diese Vorgehensweise ist auch bei verschlüsselten Messengern verbreitet: Der Schlüssel wird automatisch übertragen und
es wird erst einmal angenommen, dass er korrekt ist.

Man sollte sich daher immer bewusst sein, welche Vertrausenswürdigkeit man öffentlichen Schlüsseln abhängig von der Lieferkette/Beschaffungsmethode anrechnen möchte. Um die Sicherheit weiter zu erhöhen, gibt es verschiedene Methoden einen Schlüssel auf dessen Vertrauenswürdigkeit hin zu überprüfen. Einige davon möchten wir nachfolgend vorstellen.

## 3. Schlüsselüberprüfung mit Fingerabdrücken

Um Schlüssel zu vergleichen, ohne den ganzen Schlüssel zu kopieren, gibt es Fingerabdrücke bzw. Fingerprints. Diese haben den Vorteil,
dass sie kompakter als der Schlüssel sind und sich auf einen Zettel oder eine Visitenkarte schreiben bzw. drucken lassen.
Das Lieferkettenproblem löst das natürlich nicht, der Zettel/die Visitenkarte sollte dann schon vom Schlüsselinhaber kommen.

Der Fingerabdruck ist nicht im Schlüssel gespeichert, sondern wird aus ihm berechnet. Wie das genau funktioniert, wird im
[RFC 4880](https://datatracker.ietf.org/doc/html/rfc4880#section-12.2) beschrieben: Bei der aktuellen Version vier das Schlüsselformats
wird ein SHA1-Wert berechnet.

Den Fingerabdruck kann man auch anzeigen, ohne den Schlüssel zu importieren:

```
gpg --show-key Mister.Bob.asc
pub   rsa3072 2020-04-21 [SC] [expires: 2022-04-21]
      12659719CA0B9854292D0A8F352EF7E2607CDD39
uid                      Mister Bob <bob@anoxinon.me>
sub   rsa3072 2020-04-21 [E] [expires: 2022-04-21]
```

Wenn man sich schon die Mühe macht, sich Fingerabdrücke zu beschaffen, dann sollte man diese auch vollständig vergleichen.
Man kann sich den Fingerabdruck nicht aussuchen, aber man kann so oft einen neuen Schlüssel generieren, bis einem der Fingerabdruck gefällt.
Weniger Stellen bedeuten weniger Versuche, bis diese einen bestimmten Wert haben:
Um einen bestimmten Wert bei der ersten Stelle zu erreichen, benötigt man im Mittel 16 Versuche, für zwei Stellen schon 16\*16 = 256 Versuche.
Um für alle Stellen bei Version vier einen bestimmten Wert zu erreichen, bräuchte man 16^40 = 1,461501637×10⁴⁸ Versuche, die nicht realistisch möglich sind.

## 4. Vertrauen bis Vertrauen⁵

Neben der direkten Prüfung eines Schlüssels kann man auch darauf zurückgreifen, was andere zu einem Schlüssel sagen.

Bei HTTPS-Verbindungen zu Webseiten wird das Konzept der Zertifizierungsstellen verwendet: Eine Zertifizierungsstelle
prüft, dass eine Webseite einer Organisation gehört und stellt dieser ein Zertifikat aus. Der Browser vertraut (ggf. über
Umwege) der Zertifizierungsstelle und der Benutzer vertraut (oft unwissentlich) dem Browserhersteller, dass er die
Zertifizierungsstellen gezielt und sinnvoll gewählt hat.

Dieses Grundprinzip kommt auch beim Web of Trust, kurz WoT zum Einsatz. Der entscheidende Unterschied ist aber, dass es keine
gegebenen Zertifizierungsstellen gibt, sondern jeder angeben kann, wen er überprüft hat. OpenPGP orientiert sich dann
an den Einschätzungen von denjenigen, denen man vertraut.
Die [Dokumentation](https://www.gnupg.org/gph/en/manual/x334.html) beschreibt folgende Bedingungen, mit denen ein Schlüssel gültig ist:

- er wurde von einem selbst bestätigt
- er wurde von einem "fully trusted key" bestätigt
- er wurde von drei "marginally trusted keys" bestätigt

Zusätzlich ist die Anzahl der Vertrauensebenen auf fünf begrenzt.

Bei einem intensiven PGP-Benutzer sammeln sich viele Schlüssel an, wodurch die Anwendung dieser Regeln einige Ressourcen benötigen kann.
Das ist einer der Gründe, warum dieser Prozess manuell mit `` gpg --update-trustdb`` durchgeführt werden muss. Ein anderer Grund ist, dass
man dabei gefragt wird, wie sehr man einer Person vertraut, wenn man das noch nicht angegeben hat.

## 5. Schlüssel signieren

Angenommen, wir haben geprüft, dass ein Schlüssel zur entsprechenden Person gehört (ggf. auch unter Nutzung eines amtlichen Ausweisdokuments)
und wir wollen das nun speichern. Dann führt man den enstprechenden Befehl aus:

```
gpg --default-key 7FA1EB8644BAC07E7F18E7C9F121E6A6F3A0C7A5 --ask-cert-level --sign-key 12659719CA0B9854292D0A8F352EF7E2607CDD39
sec  rsa3072/352EF7E2607CDD39
     erzeugt: 2020-04-21  verfällt: 2022-04-21  Nutzung: SC
     Vertrauen: ultimativ     Gültigkeit: ultimativ
ssb  rsa3072/87EBA45CD82A5C31
     erzeugt: 2020-04-21  verfällt: 2022-04-21  Nutzung: E
[ ultimativ ] (1). Mister Bob <bob@anoxinon.me>
gpg: "7FA1EB8644BAC07E7F18E7C9F121E6A6F3A0C7A5" wird als voreingestellter geheimer Signaturschlüssel benutzt
sec  rsa3072/352EF7E2607CDD39
     erzeugt: 2020-04-21  verfällt: 2022-04-21  Nutzung: SC
     Vertrauen: ultimativ     Gültigkeit: ultimativ
 Haupt-Fingerabdruck  = 1265 9719 CA0B 9854 292D  0A8F 352E F7E2 607C DD39
     Mister Bob <bob@anoxinon.me>
Dieser Schlüssel wird 2022-04-21 verfallen.
Wie genau haben Sie überprüft, ob der Schlüssel, den Sie jetzt beglaubigen
wollen, wirklich der o.g. Person gehört?
Wenn Sie darauf keine Antwort wissen, geben Sie "0" ein.
   (0) Ich antworte nicht. (default)
   (1) Ich habe es überhaupt nicht überprüft.
   (2) Ich habe es flüchtig überprüft.
   (3) Ich habe es sehr sorgfältig überprüft.
Ihre Auswahl? ('?' für weitere Informationen):
```

Die Beglaubigung kann exportiert und dem Inhaber des Schlüssels gesendet werden, damit dieser diese für das WOT nutzen kann.
Dabei gibt es die Vorgehensweise, die Beglaubigung verschlüsselt zu senden, um zu testen, ob er/sie die mit dem Schlüssel
verschlüsselten Nachrichten auch lesen kann.

## 6. TOFU

Nein, hier geht es nicht ums Essen, sondern um Trust on First Use, was eine alternative zum WOT darstellt.
In einer Publikation, an der Werner Koch, der Betreuer von OpenPGP mitgearbeitet hat, heißt es
"TOFU offers better practical protection from active MitM attacks for OpenPGP users than the major alternatives, the
WoT and X.509-style PKI. Concretely, whereas the WoT offers better theoretical protection than TOFU, in practice
it requires too much user support" (https://gnupg.org/ftp/people/neal/tofu.pdf).

Die Aktivierung erfolgt mit einem Eintrag in der Konfigurationsdatei, also z.B. unter ``.gnupg/gpg.conf``: ``trust-model tofu``.
Das ist alles. Dann werden Schlüssel verwendet, wenn es keine Widersprüche gibt.

## 7. Fazit

Neben der Beschaffung von Schlüsseln muss man diese noch überprüfen oder annehmen, dass diese gültig sind. Dabei gibt es mehrere
Möglichkeiten. Welche "richtig" ist, muss jeder für sich selbst entscheiden.
