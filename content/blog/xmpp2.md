---
title: XMPP - Teil 2 - XMPP etwas genauer
date: "2023-07-29T12:00:00+00:00"
tags:
- Anfänger
categories:
- Freie Software
banner: "/img/xmpp/xmpp-series-thumbnail.png"
description: Wir gucken uns XMPP mal etwas genauer an
series: xmpp
---

**Inhaltsverzeichnis:**

1. [Konto](#account)
1. [Geräte, Priorität und Routing](#geräte-priorität-und-routing)
1. [Nachrichten, Chat, Gruppenchats und Schlagzeile](#nachrichten-chat-gruppenchats-und-schlagzeile)
1. [Message Archive Management](#message-archive-management)
1. [Gruppenchats](#gruppenchats)

## Konto

### Kontaktliste

Ein XMPP Konto hat eine Kontaktliste (*roster*). Diese ist wie folgt in
[RFC-6121](https://datatracker.ietf.org/doc/html/rfc6121#section-2) definiert.

>   In XMPP, a user's roster contains any number of specific contacts.  A
>   user's roster is stored by the user's server on the user's behalf so
>   that the user can access roster information from any device.  When
>   the user adds items to the roster or modifies existing items, if an
>   error does not occur then the server SHOULD store that data
>   unmodified if at all possible and MUST return the data it has stored
>   when an authorized client requests the roster.

Die Kontaktliste eines XMPP-Kontos liegt auf dem jeweiligen Server des
Anbieters. Da die Kontakte auf dem Server abgelegt sind, bekommt man die
Änderungen an der Kontaktliste auf allen Geräten zur Verfügung gestellt wenn
sich der Benutzer am Konto anmeldet und seine Kontaktliste abfragt.

### Persönliche Ereignisse

Eine sehr hilfreiche Erweiterung von XMPP ist das *Personal Eventing Protocol*
(kurz: PEP) welches in [XEP-0163](https://xmpp.org/extensions/xep-0163.html)
definiert ist.

> Personal eventing provides a way for a Jabber/XMPP user to send updates or
> "events" to other users, who are typically contacts in the user's roster. An
> event can be anything that a user wants to make known to other people, such
> as those described in User Geolocation (XEP-0080), User Mood (XEP-0107), User
> Activity (XEP-0108), and User Tune (XEP-0118).

Die Idee von PEP ist Informationen (beispielsweise Ereignisse) an seine Kontakte
mitzuteilen. Das kann der aktuelle Standort sein oder ein Hinweis über seine
aktuelle Laune (glücklich, sauer, hungrig,....). Es wird auch für technische
Informationen verwendet, beispielsweise für den Schlüsselaustausch bei
Verschlüsselungen.

### Visitenkarte

Im Konto kann man auch eine vCard hinterlegen. Die vCard ist eine persönliche,
digitale Visitenkarte.

![Screenshot gajim zeigt eine vCard](/img/xmpp/xmpp-2-vcard.png)

## Geräte, Priorität und Routing
XMPP ist mehrgeräte-fähig, man kann sein Konto somit auf verschiedenen
Geräten nutzen (Smartphone, PC, Laptop oder innerhalb einer Web-Anwendung).

### XMPP-Adresse und Ressource

Bei der Herstellung einer Verbindung von einem Client zu einem Server, gibt die
Client-Anwendung einen Namen der Ressource mit oder bekommt diesen vom Server
zugewiesen. Diese Ressource erweitert die XMPP Adresse. Die *bare* Adresse ist
diejenige ohne Ressource. XMPP Adressen mit Ressource bezeichnet man als
*full* Adressen. Zur Verdeutlichung machen wir ein Beispiel. Wir haben drei
Geräte und nutzen auf allen drei Geräten die gleiche XMPP Adresse. Wir geben den
Clients auf den Geräten die Ressourcennamen **Laptop**, **Smartphone**,
**Workstation**. Unsere XMPP Adresse ist `alice@anoxinon.me`.  Wenn wir uns auf
allen drei Geräten anmelden, haben wir für unsere *bare* XMPP Adresse
`alice@anoxinon.me` drei *full* XMPP Adressen

- `alice@anoxinon.me/Laptop`
- `alice@anoxinon.me/Smartphone`
- `alice@anoxinon.me/Workstation`

Warum dies heute für die meisten Benutzer eine untergeordnete Rolle spielt,
werden wird später noch erklären. Die Konzepte werden hier erläutert um ein
besseres Grundverständnis des XMPP-Protokolls zu vermitteln.

### Priorität

Das Grundkonzept wie es im RFC beschrieben ist [sendet Nachrichten nur an ein
Geräte oder Geräte die online sind](https://datatracker.ietf.org/doc/html/rfc6121#section-8.5.2.1.1). An
welches es gesendet wird ist abhängig von seiner Priorität. Mit dem Anmelden am
XMPP-Server wird eine Priorität übergeben. Das Geräte welches gerade mit der
höchsten Priorität online ist, bekommt die Nachrichten zugestellt.

- Priorität 10 `alice@anoxinon.me/Laptop`
- Priorität 30 `alice@anoxinon.me/Smartphone`
- Priorität 20 `alice@anoxinon.me/Workstation`

Ist beispielsweise das Laptop mit Priorität 10 online, werden Nachrichten an
dieses Geräte gesendet. Melden wir uns mit der Workstation unter Priorität 20
an, werden zukünftige Nachrichten an dieses Gerät geschickt bis es offline geht
oder durch die Priorität 30 vom Smartphone überholt wird. Die Verarbeitung der
Nachrichten ist genaugenommen abhängig vom Typ der Nachricht.

![Screenshot profanity Priority pro presence](/img/xmpp/xmpp-2-Priority.png)

### Carbons

Einige Nutzer möchten jedoch, dass Nachrichten an alle Geräte geschickt werden
welche gerade online sind. Hierfür wurde das Protokoll um [XEP-0280: Message
Carbons](https://xmpp.org/extensions/xep-0280.html) erweitert. Dieses wird auch
von "bekannten" XMPP Clients standardmäßig aktiviert. Hat der Client Carbons
aktiviert, bekommt er die gesamte Kommunikation als Kopie mitgeteilt, auch wenn
die Kommunikation auf einem anderen Gerät erfolgt.

Nachrichten, die nicht zugestellt werden können, da kein Gerät online ist,
werden auf dem Server als offline-Nachricht gespeichert. Geht eines unserer
Geräte online, werden die offline-Nachrichten an diese Ressource geschickt.

## Nachrichten, Chat, Gruppenchats und Schlagzeile

Unterschieden werden 4 [Arten von
Nachrichten](https://datatracker.ietf.org/doc/html/rfc6121#section-5.2.2). Den
Typ Fehlerfall vernachlässigen wir in diesem Artikel.

- Normale Nachrichten (normal)
- Nachrichten in einer Konversationen (chat)
- Nachrichten in einem Gruppenchat (groupchat)
- Schlagzeile (Headline, Warnung, Benachrichtigung)

Normale Nachrichten sind Einzelnachrichten. Sie können auch einen Betreff haben.
Chatnachrichten sind Konversationen zwischen zwei Teilnehmern. Multi-User-Chats
(MUC) werden für Chatgruppen verwendet. Schlagzeilen können als Warnung,
Benachrichtigung, Informationen dienen.

![Screenshot von gajim mit einem Fenster zum versenden von Einzelnachrichten](/img/xmpp/xmpp-2-message.png)

## Message Archive Management
[Message Archive Management](https://xmpp.org/extensions/xep-0313.html) ist eine
Erweiterung von XMPP und wird mit MAM abgekürzt. Braucht man MAM? Ein klares
Jein. MAM wird zum Chatten nicht benötigt, wir wollen es aber dennoch. Diese
Erweiterung sorgt für das serverseitige Speichern von Nachrichten. I.d.R. ist
diese Speicherung von Nachrichten zeitlich begrenzt, was jedoch serverseitig
einstellbar ist.

Werden Nachrichten eine zeitlang auf dem Server vorgehalten, kann ein Client
auch nachträglich Nachrichten aus dem Archiv anfragen. Damit können
Nachrichten empfangen werden, auch wenn das Gerät nicht online war und deshalb
Nachrichten an ein anderes Geräte geschickt wurden.

## Gruppenchats
Gruppenchats werden in der Spezifikation Multi-User-Chat (MUC) genannt. Es
handelt sich bei Gruppenchats um eine Erweiterung welche in
[XEP-0045](https://xmpp.org/extensions/xep-0045.html)
spezifiziert ist. Ein Gruppenchat hat eine eindeutige XMPP Adresse
(beispielsweise `vereinsname@chat.domain.tld`) und kann einen Namen, eine
Beschreibung und ein Thema haben. Ferner hat ein Gruppenchat die folgenden
Eigenschaften.

- Anonymität
- Teilnehmer
- Sichtbarkeit
- Persistenz
- Archivierung

Diese Einstellungen können vom Besitzer der Gruppe geändert werden.

Ist eine Gruppe semi-Anonym, so können nur die Moderatoren die XMPP Adresse der
Teilnehmer sehen. Die Teilnehmer untereinander sehen nur den Nickname, jedoch
nicht die gesamte XMPP-Adresse. Der Nickname lässt sich pro Gruppenchat vom
Teilnehmer wählen. Da die Moderatoren (Besitzer und Administratoren der Gruppe)
die richtige XMPP Adresse einsehen können, wird es semi-anonym bezeichnet. In
nicht-anonymen Gruppe kann jeder Teilnehmer die Adresse der anderen Teilnehmer
einsehen. Somit ist in nicht anonymen Gruppe auch eine Ende-zu-Ende
Verschlüsselung möglich, was bei semi-anonymen Gruppen nicht der Fall ist.

Wer einem Gruppenchat beitreten darf, kann auch eingestellt werden. Ist der
Gruppenchat öffentlich, kann jeder diesen Gruppenchat betreten. Es ist aber auch
möglich einen Gruppenchat auf bestimmte XMPP-Adressen einzuschränken. Hier kann
abhängig von den Einstellungen des Raumes jeder Teilnehmer weitere Personen in den
Raum einladen oder es liegt ausschließlich in der Verantwortung der Moderatoren.

Es gibt bei XMPP ein [Service Discovery](https://xmpp.org/extensions/xep-0030.html). 
Damit lassen sich existierende Gruppenräume abfragen. Der Besitzer eines
Gruppenchats kann entscheiden ob der Raum in der Liste sichtbar ist oder nicht.
Dies beutet ein Raum ist sichtbar oder eben versteckt.

Auch die Persistenz ist eine Eigenschaft. Sie entscheidet was mit den Gruppenchats
passiert, wenn dieser keine Teilnehmer mehr hat. Ist der Raum dann wieder frei
verfügbar und kann von anderen genutzt werden oder bleibt dieser persistent und
ist somit für die weitere Nutzung reserviert.

![Ein Screenshot, das die MUC-Einstellungen in gajim zeigt](/img/xmpp/xmpp-2-muc-settings.png)

