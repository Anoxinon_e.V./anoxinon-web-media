---
title: OpenPGP - Teil 4 - Dokumente verschlüsseln und signieren
date: "2022-03-01T19:30:00+00:00"
tags:
- Fortgeschritten
categories:
- Sicherheit
- Datenschutz
- Freie Software
banner: "/img/thumbnail/openpgp-final.png"
series: pgp
description: >
  Neben E-Mails können mit OpenPGP auch Dokumente verschlüsselt und signiert werden.
  In diesem Artikel zeigen wird das am Beispiel von LibreOffice, das eine OpenPGP-Integration hat.
---

**Inhaltsverzeichnis:**

1. [Einleitung](#einleitung)
2. [Signaturen in LibreOffice Writer](#signaturen-in-libreoffice-writer)
  - [Dokumente signieren](#dokumente-signieren)
  - [Signaturen anzeigen](#signaturen-anzeigen)
3. [Ver-/Entschlüsselung in LibreOffice Writer](#ver-entschlüsselung-in-libreoffice-writer)
  - [Dokumente verschlüsseln](#dokumente-verschlüsseln)
  - [Dokumente entschlüsseln](#dokumente-entschlüsseln)
4. [Fazit](#fazit)
5. [Literatur und Quellen](#literatur-und-quellen)

## Einleitung

LibreOffice ist eine freie und quelloffene Büroprogrammsammlung und eine der bekanntesten Alternativen zum weitverbreiteten Microsoft Office. Sie umfasst unter anderem:

* Writer, eine Textverarbeitung
* Calc, eine Tabellenkalkulation
* Impress, ein Präsentationsprogramm
* Draw, ein Zeichnenprogramm

Auch Dateien, die mit LibreOffice erstellt wurden, können mit OpenPGP signiert und verschlüsselt werden. Dabei ist OpenPGP direkt in die Oberfläche der LibreOffice-Programmsammlung integriert und kann so komfortabel verwendet werden. Wir zeigen das Vorgehen, um eine LibreOffice-Datei mit OpenPGP zu signieren und zu verschlüsseln am Beispiel von LibreOffice Writer.

---
**Teil einer Artikelserie:**

{{< series-parts >}}

---

## Signaturen in LibreOffice Writer

OpenPGP kann in LibreOffice für die Signatur von Dokumenten verwendet werden, ebenfalls kann man direkt in der LibreOffice-Oberfläche bestehende Signaturen und den Signaturstatus einsehen. Wie das funktioniert, zeigen wir folgend.

### Dokumente signieren

Um eine geöffnetes Dokument zu signieren, wählen wir ``Datei`` → ``Digitale Signaturen`` → ``Digitale Signaturen`` im Menü.
Die Datei muss gespeichert werden, bevor sie signiert werden kann.

Wir bekommen ein Fenster angezeigt, welches alle bestehenden Signaturen des Dokuments anzeigt und zu Beginn noch leer ist.

![die Liste der Signaturen einer Datei in LibreOffice; hier ist diese noch leer](/img/pgp/lo/01_common_theme.png)

Entscheidend ist dort die Möglichkeit das Dokument mit der Schaltfläche `Dokument signieren` zu signieren. 
Es werden alle eigenen (privaten) Schlüssel angeboten und man kann wählen, welchen man für die Signatur verwenden möchte:

![das LibreOffice-Dialog zur Auswahl des Schlüssels, mit dem signiert werden soll](/img/pgp/lo/02_common_theme.png)

Wir wählen den Schlüssel aus, den wir zum Signieren des Dokuments verwenden wollen.

Bestätigt man mit Klick auf `Signieren`, wird das Dokument nach der Eingabe der Passphrase des OpenPGP-Schlüssels signiert. Das führt zu einem neuen Eintrag in der Liste unter ``Digitale Signaturen``.

### Signaturen anzeigen

Ebenfalls über ``Datei`` → ``Digitale Signaturen`` → ``Digitale Signaturen`` - und wie schon im vorherhigen Absatz angerissen - lassen sich alle Signaturen des Dokuments anzeigen. In unserem Beispiel ist dort die Signatur, die wir eben erstellt haben zu sehen:

![die Liste der Signaturen einer Datei in LibreOffice; hier mit einer Signatur](/img/pgp/lo/03_common_theme.png)

Direkt nach dem Öffnen eines Dokuments wird angezeigt, ob die Signatur gültig ist oder ob es sich um eine ungültige Signatur handelt. Ob eine Signatur gültig ist oder nicht wird dabei von dem Signatursystem, in unserem Fall OpenPGP bestimmt. Gültig ist eine Signatur beispielsweise, wenn du den zugehörigen öffentlichen Schlüssel im Schlüsselbund gespeichert hast und dieser gültig ist.

![LibreOffice mit einem Banner, der anzeigt, dass die Datei signiert wurde](/img/pgp/lo/04_common_theme.png)

Die Signaturen lassen sich auch über ``Datei`` → ``Eigenschaften`` öffnen:

![das LibreOffice-Eigenschaften-Dialog bei einer signierten Datei](/img/pgp/lo/05_common_theme.png)

## Ver-/Entschlüsselung in LibreOffice Writer

Neben dem digitalen Signieren lassen sich Dokumente in LibreOffice auch mithilfe von OpenPGP ver- und entschlüsseln. Dabei kommt, wie oft in der Praxis, ein hybrides Verschlüsselungsverfahren zum Einsatz. Das bedeutet, dass Dokument an sich wird mit einem symmetrischen Verschlüsselungsverfahren verschlüsselt und OpenPGP dient dann nur dazu, den symmetrischen "Session"-Schlüssel zu verschlüsseln. Beides ist in der LibreOffice-Datei gespeichert. So weden die Vorteile beider Verfahren kombiniert und eine effiziente Ver-/Entschlüsselung möglich.

## Dokumente verschlüsseln

Um die OpenPGP-Verschlüsselung zu verwenden, steht die Option ``Mit GPG-Schlüssel verschlüsseln`` zur Verfügung und wird beim Abspeichern des Dokuments als Checkbox angeboten. Wenn das Dokument zuvor schon abgespeichert war, bekommt man die Option ebenfalls durch Wählen von `Datei` → `Speichern unter` angeboten und kann so eine verschlüsselte Kopie anlegen.

![das Speicherdialog von LibreOffice mit einem gesetzten Haken bei der Verschlüsselungsoption](/img/pgp/lo/07_common_theme.png)

Wie auch sonst muss OpenPGP erfahren, welche öffentlichen Schlüssel verwendet werden sollen.
Bei einer E-Mail gibt es Empfänger:innenangaben, die dafür verwendet werden können.
Bei einer Datei gibt es die nicht, sodass man diese auswählen muss:

![das Empfänger:innenauswahldialog bei der Verschlüsselung in LibreOffice](/img/pgp/lo/08_common_theme.png)
![das Empfänger:innenauswahldialog bei der Verschlüsselung in LibreOffice - jetzt mit ausgewählten Empfängern](/img/pgp/lo/09_common_theme.png)

Mit Klick auf `Verschlüsseln` wird das Dokument verschlüsselt abgespeichert.
Nur die Personen, die soeben ausgewählt wurden, können das Dokument entschlüsseln und somit lesen. 

**Beachte:** Das schließt auch dich selbst ein! Wenn du das Dokument nicht auch für dich verschlüsselt hast, wirst du es nicht mehr öffnen können!

### Dokumente entschlüsseln

Um ein verschlüsseltes Dokument zu entschlüsseln, ist keine dedizierte Aktion notwendig. Man wird beim Öffnen automatisch zur Eingabe der Passphrase aufgefordert, woraufhin man ganz normal mit dem Dokument arbeiten kann.
Voraussetzung ist natürlich, dass OpenPGP eingerichtet ist und man über das Schlüsselpaar bzw. den privaten Schlüssel verfügt, für das das Dokument verschlüsselt wurde.

Im Hautpfenster von LibreOffice werden verschlüsselte Dokumente mit einem kleinen Schloss angezeigt. Beispielsweise auch unser zuvor verschlüsseltes Beispieldokument:
![libreoffice-10.png](/img/pgp/lo/10_common_theme.png)

## Fazit

Grundsätzlich kann OpenPGP mit beliebigen Dateien arbeiten. Komfortabler wird es allerdings, wenn das
Anwendungsprogramm mit OpenPGP zusammenarbeitet, so wie es z.B. bei LibreOffice der Fall ist. Durch die Integration von OpenPGP in die Oberfläche von LibreOffice ist es so niedrigschwellig und ohne nennenswerte Einschränkung möglich, mit Verschlüsselung und Signaturen bei elektronischen Dokumenten zu arbeiten.

## Literatur und Quellen

- [LibreOffice Help: Applying Digital Signatures](https://help.libreoffice.org/latest/en-US/text/shared/guide/digitalsign_send.html)
- [LibreOffice Help: Encrypting Documents with OpenPGP](https://help.libreoffice.org/latest/en-US/text/shared/guide/openpgp.html)