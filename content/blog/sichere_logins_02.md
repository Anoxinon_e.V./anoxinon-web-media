---
title: Sichere Logins - Teil 2 - Passwortmanager
date: "2021-07-01T00:00:00+00:00"
tags:
- Anfänger
categories:
- Sicherheit
banner: "/img/thumbnail/sichere_logins_01.png"
series: sichere_logins
description: Logins sind ein elementarer Bestandteil in der digitalen Welt von heute. Was leider auch ein Bestandteil davon ist, sind immer wieder Schlagzeilen über geknackte Konten und den Schabernack. Auch weil Endnutzerinnen und Endnutzer unsichere Logins nutzen. Wie Du das vermeiden kannst, wird Dir in diesem Beitrag verständlich erklärt. 
---

# Sichere Logins - Teil 2 - Passwortmanager

**Inhaltsverzeichnis:**

1. [Einleitung](#einleitung)
2. [Das Passwortchaos](#das-passwortchaos)
3. [Was sind Passwortmanager?](#was-sind-passwortmanager)
4. [Bitwarden](#bitwarden)
	- [Vor- und Nachteile](#auf-einen-blick-vor-und-nachteile-von-bitwarden)
	- [Einrichtung](#einrichtung)
	- [Verwendung](#verwendung)
6. [KeePassXC](#keepassxc)
	- [Das KeePass-Format und die KeePass-Apps](#hinweis)
	- [Vor- und Nachteile](#auf-einen-blick-vor-und-nachteile-von-keepassxc)
	- [Einrichtung](#einrichtung-keepassxc)
	- [Verwendung](#verwendung-1)
8. [Literatur und Quellen](#literatur-und-quellen)

## Einleitung

Die Bildschirmsperre beim PC und Laptop oder die Anmeldung beim E-Mail-Konto, beim Online-Banking oder beim Online-Shopping - heutzutage nutzt fast jeder unzählige digitale Geräte und Dienste, bei denen man sich in irgendeiner Weise einloggen - authentifizieren - muss. Das ist auch gut so, denn nur so kann sichergestellt werden, dass Deine digitalen Dienste nicht von anderen in Deinem Namen missbraucht werden. Logins sind ein elementarer Bestandteil in der digitalen Welt von heute. Was leider auch ein Bestandteil davon ist, sind immer wieder Schlagzeilen über geknackte Konten und den Schabernack, den Kriminelle damit treiben. Warum? Weil entweder Diensteanbieterinnen und Diensteanbieter ihre Dienste schlecht absichern oder die Endnutzerinnen und Endnutzer unsichere Logins nutzen. Oft ist das keine Absicht, sondern schlicht Unwissen. Gegen ersteres kannst Du nur begrenzt etwas machen, gegen letzteres schon. Genau in diese Lücke zielt diese Artikelserie - Du wirst merken, es ist im Grunde kein Hexenwerk ausreichend sichere Logins zu nutzen.

---
**Teil einer Artikelserie:**

{{< series-parts >}}

---

## Das Passwortchaos

Im [letzten Artikel über sichere Logins](/blog/sichere_logins_01/) ging es darum, sichere Passwörter zu wählen. Auch wenn wir aufgezeigt haben, wie man diese ebenso merkbar gestalten kann, ist doch klar, dass das nur bis zu einer bestimmten Anzahl an Passwörtern funktioniert, weit zu wenig, wenn man für jeden Dienst ein unterschiedliches Passwort wählt - und das sollte man machen. Daher soll sich dieser Artikel eine Lösung dieses Problems erklären: Passwortmanager.

## Was sind Passwortmanager?

Passwortmanager sind im Grunde nichts anderes, als Programme, die eine Liste mit Deinen Passwörtern speichern. Das Prinzip dabei ist, dass man sich **ein** Passwort (**genannt Haupt-Passwort oder Master-Passwort**) erstellt, welches dem Passwortmanager zugewiesen und das bei jedem Start eigegeben wird. Alle anderen Zugänge sind dann im Passwortmanager gespeichert, man muss sich also nur noch **ein einziges** Passwort merken. Je nach Passwortmanager bekommt man dadurch einiges an Komfort:

* Speichern von allen Passwörtern und die Informationen, die zu diesen gehören: Benutzernamen, Websites usw...
* Zufallsgenerator für Passwörter
* Beliebiges Katogorisieren von Passwörtern (Shopping, Social Media, Arbeit, Finanzen)
* Automatisches Ausfüllen von Passwörtern im Browser (Firefox, Chrome etc...)
* Ausfüllen von Passwörtern in anderen Programmen per Tastenkombination
* Und so weiter...

Passwortmanager machen es also nicht nur möglich für jeden Dienst ein anderes Passwort zu wählen, sondern bringen auch einiges an Komfort. Was aber auch klar wird, ein Programm, dem man alle eigenen Zugangsdaten anvertraut sollte **vertrauenswürdig und sicher** sein. Auf was es dabei ankommt:

* Das Passwort zum Entsperren des Passwortmanagers sollte besonders sicher sein (siehe [unsere Tipps dazu](/blog/sichere_logins_01))
* Der Passwortmanager sollte **Ende-zu-Ende-verschlüsselt** sein, das heißt, dass der Passwortmanager Deine Passwörter auf Deinem PC verschlüsselt, damit sie niemand lesen kann, der nicht Dein Haupt-Passwort kennt, selbst wenn er Zugriff auf die Datei des Passwortmanagers bekommen sollte
* Der Passwortmanager sollte quelloffen sein, das heißt, dass der Bauplan des Passwortmanagers offen einsehbar ist und so von Expert:innen verifiziert werden kann
* Er sollte aktiv gepflegt werden, damit Sicherheitslücken schnellstmöglich behoben werden
* Außerdem ist es von Vorteil, wenn der Passwortmanager einen gewissen Bekanntheitsgrad hat, damit mögliche Lücken im bestenfalls schneller entdeckt werden. Im Idealfall werden regelmäßig unabhängige Sicherheitsprüfungen durchgeführt, die den Passwortmanager auf Herz und Nieren prüfen

Da das doch einige Dinge sind, die man beachten sollte, werden wir im nachfolgenden zwei Passwortmanager mit deren Vor- und Nachteilen vorstellen und Dich durch den Einrichtungsprozess führen. Wenn Du das einmal geschafft hast und Dich an die Verwendung gewöhnt hast, wirst Du nicht mehr darauf verzichten wollen - denn vergessene oder schwache Passwörter gehören damit der Vergangenheit an.  

Wir werden nachfolgend auf **Bitwarden und KeePassXC** als Passwortmanager eingehen. Wenn Du Dich nur für einen der beiden Kandidaten interessierst, kannst Du nachfolgend direkt zum passenden Abschnitt springen:

- [Zu Bitwarden](#bitwarden)
- [Zu KeePassXC](#keepassxc)

Solltest Du Dich nicht entscheiden können, hier unsere Empfehlung:

- Verwende **Bitwarden** als technisch unversierte:r Nutzer:in. Einmal eingerichtet, funktioniert Bitwarden auf allen Plattformen gleich und Du musst Dich um nichts mehr kümmern.
- Verwende **KeePassXC** wenn Dir eine etwas anspruchsvollere Einrichtung nichts macht und Du unabhängig von einem bestimmten Unternehmen sein möchtest.

## Bitwarden

Bitwarden ist ein quelloffener und cloudbasierter Passwortmanager aus den USA. Er ist für alle gängigen Betriebssysteme - Windows, MacOS, Linux, Android und iOS - verfügbar. Die Zugangsdaten werden automatisch synchronisiert und sind auch im Browser verfügbar. Sowohl auf Mobilgeräten als auch im Browser kann man sich die Zugangsdaten automatisch ausfüllen lassen. 

Trotz dessen, dass es sich um eine cloudbasierte Lösung handelt - die Passwortdatei wird also auf den Servern von Bitwarden gespeichert - kann man das für unbedenklich halten: Die Apps sind quelloffen und alle Daten werden vor dem verlassen des eigenen Geräts mit dem nur Dir bekannten Masterpasswort verschlüsselt. Darüberhinaus wurden bei Bitwarden schon unabhänige Sicherheitsprüfungen durchgeführt. Dennoch sollte man sich im klaren sein, dass man davon abhängig ist, dass Bitwarden weiterbesteht und den Dienst weiter kostenlos anbietet. Bei regelämßgen Backups sollte das aber unbedenklich sein - wie das funktioniert, wird im weiteren Verlauf beschrieben. Wer das technische Wissen hat kann die Bitwarden-Server bei Bedarf auch selber betreiben, denn auch sie sind quelloffen.

Die schon genannten Grundfunktionen sind bei Bitwarden kostenlos, es gibt aber auch erweiterte Funktionen, die nur im Abo verfügbar ist. Das ist aber grundsätzlich nichts schlechtes, auch die Entwickler:innen von Bitwarden müssen von irgendetwas leben.

Bitwarden kann zum Beispiel auch für Unternehmen eine gute Wahl sein, mit Funktionen wie dem Teilen von Passwörtern mit Personen oder Teams.

### Auf einen Blick - Vor- und Nachteile von Bitwarden

**Vorteile:**

- **Ende-zu-Ende-Verschlüsselt**: Deine Zugangsdaten können nur von Dir, wenn das Masterpasswort bekannt ist, eingesehen werden.
- **Quelloffen**: Der Bauplan von Bitwarden ist einsehbar und kann von Expert:innen geprüft werden
- **Sicherheitsaudits:** Bitwarden wurde schon mehrfach von unabhängigen Expert:innen auf dessen Sicherheit getetstet
- **Für alle gängigen Plattformen verfügbar**: Es gibt Programme für Windows, MacOS, Linux, iOS und Android
- **Autofill im Browser**: Durch Addons integriert sich Bitwarden nahtlos in den Internet-Browser Deiner Wahl
- **Automatisch Synchronisierung von Zugangsdaten (cloudbasiert)**: Egal auf welchem Gerät Du Deine Zugangsdaten anlegst, Bitwarden synchronisiert sie automatisch auf alle Deine anderen Geräte
- **Kollaborations-Funktionen, um Passwörter zu teilen**: Du kannst Passwörter mit anderen Nutzer:innen von Bitwarden zur gemeinsamen Nutzung freigeben

**Nachteile:**

- **Automatische Synchronisierung von Zugangsdaten (cloudbasiert)**: Die **verschlüsselten** Zugangsdaten werden auf Bitwarden-Server hochgeladen. Das bringt Komfort, jedoch steht und fällt die Sicherheit mit der Verschlüsselung
- **Abhängigkeit von einem Unternehmen**: Die Entwicklung von Bitwarden wird von einem Unternehmen getrieben, wenn dieses Bitwarden in Zukunft nicht mehr (kostenlos) anbietet, muss man u.U. den Passwortmanager wechseln. 

### Einrichtung

Die Einrichtung funktioniert recht unkompliziert, die passenden [Download-Links finden sich direkt bei Bitwarden](https://bitwarden.com/download/). Lade Dir dort die passende Option herunter und klicke Dich durch den Setup-Prozess. Solltest Du Dir unsicher über die Einrichtung sein, kannst Du nachfolgend zu Details zur passenden Plattform springen. Ansonsten kannst Du mit der [Verwendung fortfahren](#verwendung).

**Desktop-PC:**

- [Windows-Installation](#windows)
- [MacOS-Installation](#macos)
- [Linux-Installation](#linux)

**Internet-Browser:**

- [Addon für Mozilla Firefox](#mozilla-firefox)
- [Addon für Google Chrome](#google-chrome)
- [Addon für Microsoft Edge](#microsoft-edge)
- [Addon für Apple Safari](#apple-safari)

**Mobilgerät:**

- [Android-Installation](#android)
- [iPhone/iOS-Installation](#ios)

#### Konto erstellen

Bevor Bitwarden verwendet werden kann, brauchst Du ein Konto. Dieses kannst Du Dir bei [Bitwarden registrieren](https://vault.bitwarden.com/#/register). Für die Registration ist eine E-Mail-Adresse und das Master-Passwort zwingend erforderlich. Es ist besonders wichtig, ein sicheres Master-Passwort zu wählen und es sich zu merken. Es wird verwendet, um die Daten im Passwortmanager zu verschlüsseln und kann **nicht** zurückgesetzt werden, auch nicht von Bitwarden. 

{{< bildstrecke sichere_logins_02_bitwarden_account >}}

#### Installation unter Linux, Windows und MacOS

Für den Desktop PC kann [Bitwarden](https://bitwarden.com) von deren Webseite im [Download-Bereich](https://bitwarden.com/download/) heruntergeladen werden. Das funktioniert für Bitwarden prinzipiell wie für jedes andere Programm auch, die genauen Schritte werden nachfolgend beschrieben.

##### Windows

1. [**Bitwarden herunterladen**](https://vault.bitwarden.com/download/?app=desktop&platform=windows) und die Datei speichern
2. **Downloadverzeichnis öffnen**: Den Windows-Explorer öffnen, z.B. durch Druck der `Windows-Taste` + `E`  und zu Downloads navigieren
3. **Bitwarden-Installer ausführen:** Doppelklick auf die eben heruntergeladene `Bitwarden-Installer-X.XX.X.exe`-Datei machen
4. **Setup abschließen:** Nun kann man sich nach den eigenen Bedürfnissen durch den Installer klicken und erhält am Ende Bitwarden Schlüsselfertig auf den eignen Desktop 
4. **Bitwarden starten** und initial mit dem vorher erstellten Konto anmelden

{{< bildstrecke sichere_logins_02_bitwarden_installation_windows >}}

##### MacOS

1. [**Bitwarden im MacOS App Store öffnen**](https://itunes.apple.com/app/bitwarden/id1352778147)
2. **Installieren klicken** und bestätigen
3. **Bitwarden starten** und initial mit dem vorher erstellten Konto anmelden

{{< bildstrecke sichere_logins_02_bitwarden_installation_macos >}}

##### Linux

1. [**Bitwarden herunterladen**](https://vault.bitwarden.com/download/?app=desktop&platform=linux) und die Datei speichern
2. **Downloadverzeichnis öffnen**: Den Dateimanager Deiner Wahl öffnen, funktioniert oft durch Druck von `Super` + `E` und zu den Downloads navigieren
3. **Datei ausführbar machen:** Rechsklick auf die eben heruntergeladene Datei `Bitwarden-....AppImage` machen -> Eigenschaften -> Berechtigungen öffnen und als Auführbar markieren
4. **Bitwarden starten**: Nun kann Bitwarden per Doppelklick auf die Datei gestartet werden, bei Bedarf kann sie noch an andere Stelle verschoben werden und ein Startmenüeintrag erstellt werden
5. **Initiales anmelden:** Nach Start initial mit dem vorher erstellten Konto anmelden

{{< bildstrecke sichere_logins_02_bitwarden_installation_linux >}}

#### Einrichtung der Browser-Erweiterung

Mithilfe der Browser-Erweiterung kann Bitwarden automatisch Passwörter für und auf Webseiten aufüllen, erstellen und speichern. Das ist ungemein praktisch - einmal eingerichtet und verstanden, muss man nie wieder manuell Passwörter beim Online-Shopping, E-Mails schreiben und so weiter eingeben.

##### Mozilla Firefox

1. [**Zum Mozilla AddOn Store navigieren**](https://addons.mozilla.org/firefox/addon/bitwarden-password-manager/) und auf "Zu Firefox hinzufügen" klicken
2. **Initiales anmelden:** Auf das kleine blaue Schild oben rechts im Firefox klicken und mit dem vorher erstellten Konto einloggen

{{< bildstrecke sichere_logins_02_bitwarden_addon_firefox >}}

##### Google Chrome

1. **[Zum Chrome Webstore navigieren](https://chrome.google.com/webstore/detail/bitwarden-free-password-m/nngceckbapebfimnlniiiahkandclblb)** und auf "Hinzufügen" klicken
2. **Erweiterungsmenü öffnen:** Auf das kleine Puzzlesymbol klicken und Bitwarden anwählen, wahlweise mit dem kleinen Pinnadel-Symbol anpinnen
3. **Erstmaliges einloggen:** Dann kann man sich initial einloggen und loslegen

{{< bildstrecke sichere_logins_02_bitwarden_addon_chrome >}}

##### Microsoft Edge

1. [Zum Microsoft Edge AddOn Store navigieren](#) und auf "Abrufen" klicken
2. **Erstmaliges anmelden:** Auf das kleine, blaue Schild rechts oben im Edge klicken und mit dem vorher erstellten Konto einloggen

{{< bildstrecke sichere_logins_02_bitwarden_addon_edge >}}

##### Apple Safari

Das Bitwarden-Addon für Safari wird bei MacOS automatisch mit dem Bitwarden-Programm mitinstalliert. Daher muss es lediglich aktiviert werden.

1. **In die Safari-Einstellungen wechseln:** In der Menüleiste "Safari" -> "Einstellungen" anwählen
2. **Die Bitwarden-Erweiterung aktivieren:** Im Tab "Erweiterungen" das Häckchen bei Bitwarden setzen
3. **Bitwarden-Berechtigungen vergeben:** Das kleine blaue Schildsymbol von Bitwarden auswählen und den Zugriff von Bitwarden auf Webseiten erlauben
4. **Erstmaliges anmelden:** Schlussendlich mit dem eigenen Konto anmelden

{{< bildstrecke sichere_logins_02_bitwarden_addon_safari >}}

#### Installation unter Android und iOS

##### Android

1. [**Zum PlayStore navigieren**](https://play.google.com/store/apps/details?id=com.x8bit.bitwarden) und "Installieren" wählen
2. Bitwarden öffnen und anmelden - fertig!

{{< bildstrecke sichere_logins_02_bitwarden_installation_android >}}

##### iOS

1. [Zum AppStore navigieren](https://itunes.apple.com/app/bitwarden-free-password-manager/id1137397744?mt=8) und "Laden" wählen
2. Installation bestätigen und abwarten
3. Bitwarden öffnen und anmelden
4. Autofill aktivieren
	- Zu den iOS-Einstellungen gehen
	- Herunterscrollen zu Passwörtern, dort "Automatisch ausfüllen" wählen
	- Automatisch ausfüllen an sich und für Bitwarden aktivieren

{{< bildstrecke sichere_logins_02_bitwarden_installation_ios >}}

### Verwendung

Nun, da Bitwarden installiert und eingerichtet ist, können wir es verwenden.
Nachfolgend kannst Du direkt zu bestimmten Themenpunkten springen, wenn Du nicht für alles eine Beschreibung benötigst:

- [Entsperren und die Passwortdatenbank](#entsperren-die-passwortdatenbank)
- [Anlegen von Einträgen](#anlegen-von-einträgen)
- [Verwalten von Einträgen](#verwalten-von-einträgen)
- [Die Web-Browser-Erweiterung](#die-web-browser-erweiterung)
- [Backups](#backups)

#### Entsperren & die Passwortdatenbank

Der erste Schritt, der bei jedem Start von Bitwarden nötig ist, ist das Entsperren der Passwortdatenbank. 

Die Passwortdatenbank ist ein **verschlüsselter** Speicherbereich bzw. Datenablage in der die Zugangsdaten gespeichert werden können. Im Grunde kann man es sich wie einen Aktenorder vorstellen, den man in einem Tresor ablegt. Es ist die vertrauenswürdige Ablage für all Deine Passwörter und Zugangsinformationen. Daher ist besonders wichtig, diese mit einem möglichst starken Kennwort zu schützen. Dieser Schritt ist bereits bei der Einrichtung gegangen worden, beim Erstellen des Bitwarden-Kontos. Nun kann die Passwortdatenbank bei jedem Start von Bitwarden mit Deinen Zugangsdaten entsperrt werden - mit der angegebenen E-Mail-Adresse und dem zugehörigen Passwort: 

![Bitwarden Anmeldung - Entsperren der Passwortdatenbank nach dem ersten Start](/img/sichere_logins_02/bitwarden_login_e.png)

Danach landest Du auch schon direkt in der entsperrten Passwortdatenbank, in Deinem sicheren Ablagebereich für die Zugangsdaten und kannst mit der Verwendung loslegen. Anfangs wird es noch keine Einträge geben, mit der Zeit wird es sich aber hoffentlich füllen:

![Bitwarden Hauptfenster ohne jedgliche Zugangsdaten](/img/sichere_logins_02/bitwarden_hauptfenster.png) 

Wichtig zu wissen ist, dass auch wenn für die Registration eine E-Mail-Adresse vonnöten war, das Passwort nicht zurückgesetzt werden kann, Du musst es Dir unter jeden Umständen merken. Das hat den Hintergrund, dass die Passwortdatenbank von Bitwarden Ende-zu-Ende-verschlüsselt ist, die Zugangsdaten werden nur auf Deinem Computer ver- und entschlüsselt - damit haben nicht einmal die Bitwarden-Betreiber:innen Zugriff auf die von Dir verwendeten und abgespeicherten Kennwörter.

#### Anlegen von Einträgen

Der traurige Smiley nach dem initialen Start zeigt Dir schon, dass sich so eine leere Passwortdatenbank nicht gehört. Zeit also, die ersten Einträge anzulegen. 

Um einen neuen Eintrag zu erstellen, findest Du unten am Bildschirm ein Plus-Symbol. Das ist der Knopf um einen neuen Eintrag zu erstellen. Nach Druck dieses Knopfes kannst Du in der Eingabemaske Die Zugangsdaten für eines Deiner Konten eintragen:

![Bitwarden Eintrag erstellen](/img/sichere_logins_02/bitwarden_eintrag_erstellen_e.png)

1. Der Name des Eintrags. So kannst Du die Einträge später suchen, wiederfinden und zuordnen.
2. Der Nutzer:innenname für das Konto also der Loginname, den Du zum Anmelden verwendest.
3. Schließlich das Passwort für das Konto.
4. Sollte es ein neues Konto sein, kannst Du in Bitwarden direkt ein sicheres Passwort generieren lassen. Das geht über das kleine Kreissymbol neben dem Passwort-Feld beim Anlegen eines Eintrages.
5. Und die URL / Web-Adresse für das Konto. Das ist wichtig, damit Bitwarden in Deinem Browser, z.B. Firefox, automatisch Dein Passwort eintragen kann. Außerdem wird dem Eintrag dann automatisch ein kleines Bildchen angefügt, was die Übersicht erleichtert.

#### Verwalten von Einträgen

Nach einer Zeit haben sich dann hoffentlich einige Einträge in Deinem Bitwarden Passwortmanager gesammelt:

![Bitwarden Hautpfenster gefüllt mit Beispieleinträgen](/img/sichere_logins_02/bitwarden_einträge_e.png)

1. Das Plus-Symbol erlaubt Dir, immer weitere Einträge und Zugangsdaten zu speichern.
2. Das Stift-Symbol erlaubt Dir, einen Eintrag zu bearbeiten.
3. In der linken Leiste kannst Du durch alle Einträge blättern.
4. In der rechten Leiste kannst Du die Details zum aktuell angewählten Eintrag betrachten. Nützlich ist auch das kleine, aberundete Rechteck neben dem Nutzer:innenname und dem Passwort - damit lassen sich die entsprechenden Einträge kopieren, sodass Du sie in anderen Programmen wieder einfügen kannst.


#### Die Web-Browser-Erweiterung

Besonders praktisch an Bitwarden ist auch die Web-Browser-Erweiterung. Das ist ein kleines Zusatzprogramm für Deinen Web-Browser, zum Beispiel für Firefox. [Die Installation haben wir ebenfalls schon am Anfang dieses Artikels behandelt.](#mozilla-firefox)

Das Firefox-Addon steht Dir als kleines Symbol oben rechts in Deinem Firefox zur Verfügung. Dort kannst Du Dich dann analog zum Bitwarden-Desktop-Programm anmelden. In Google Chrome und anderen Web-Browsern solltest Du auch ein Icon an ähnlicher Stelle finden. Daher werden wir hier nur Bildschirmfotos für Firefox einfügen.

![Bitwarden Firefox Addon initaler Startbildschirm](/img/sichere_logins_02/bitwarden_firefox_startfenster_e.png)

Danach hast Du genauso eine Übersicht und Verwaltungsmöglichkeit über Deine gespeicherten Einträge, kannst neue Anlegen, bestehende einsehen und bearbeiten und so weiter.

![Bitwarden Firefox Addon Hauptfenster](/img/sichere_logins_02/firefox_bitwarden_tresor.png) 

Das ist soweit nichts besonderes und wurde auch zuvor schon beschrieben. Das praktische am Addon ist die Autofill-Funktion. 

Die Autofill-Funktion ermöglicht es, dass Deine Passwörter bzw. Zugangsdaten auf Webseiten automatisch ausgefüllt werden und Du sie somit nicht kopieren oder händisch eingeben musst. Dazu musst Du einfach die Webseite aufrufen und auf das Bitwarden-Symbol klicken. Dir wird dann automatisch ein passender Eintrag vorgeschlagen und nach Klick auf diesen werden die Zugangsdaten auf der Webseite augefüllt (hier am Beispiel der GLS-Webseite):

![Bitwarden Autofill auf der GLS-Webseite](/img/sichere_logins_02/bitwarden_firefox_tab_e.png)

Das automatische Erkennen funktioniert natürlich nur, wenn die URL korrekt im Eintrag hinterlegt ist. 

#### Backups

Wie in der Einleitung schon angesprochen, empfiehlt es sich, regelmäßig Backups der gespeicherten Passwörter anzulegen. So ist man abgesichert, falls Bitwarden plötzlich die Schotten dicht macht. 

Da Bitwarden ein cloudbasierter Dienst ist, muss man die eigene Passwortdatei zunächst exportieren:

1. Bitwarden auf Deinem PC öffnen und entsperren
2. Datei -> Tesor exportieren anwählen
3. Das Masterpasswort eingeben und auf das Speichern-Symbol klicken
4. Nach einer weiteren Bestätigung den Speicherort auswählen

Wichtig ist, dass die Passwortdatei an einem **sicheren** Ort gespeichert wird, denn diese enthält **alle Zugangsdaten im Klartext**. Jede:r mit Zugriff auf diese Datei kennt Deine Passwörter! Am Besten ist es daher, dass Du sie auf einem verschlüsselten Laufwerk speicherst. Wenn Du nicht weißt, wie man so etwas benutzt, dann richtet es auch ein **separater** USB-Stick, den Du an einem sicheren Ort aufbewahrst. Unter keinen Umständen solltest Du jedoch die unverschlüsselte Passwortdatei direkt auf Deinem PC lagern, dann könntest Du Dir genaus eine Excel-Datei mit Deinen Passwörtern erstellen. 

## KeePassXC

KeePassXC ist ein quelloffener, lokaler Passwortmanager, der von einer Gemeinschaft entwickelt wird. Er nutzt das quelloffene KeePass-Format. Verfügbar ist er für Windows, MacOS und Linux, für Android gibt es die unabhänige App KeePass2Android und für iOS gibt es Strongbox. Beide können mit dem einheitlichen KeePass-Dateiformat umegehen.

Im Gegensatz zu Bitwarden handelt es sich nicht um einen Cloudservice und die Zugangsdaten werden in einer lokal gespeicherten Datei vorgehalten. Um das Synchronisieren mit verschiedenen Geräten muss man sich also seperat kümmern - das kann Vor- sowie Nachteil zugleich sein. Es bietet sich zum Beispiel eine Dateicloud wie Nextcloud oder eine Lösung wie [Syncthing](/blog/syncthing/) an. Da die Datei auch verschlüsselt ist, tut es zur Not oder zum Ausprobieren aber auch Google Drive, OneDrive, Dropbox, iTunes oder ähnliches.

Genauso wie Bitwarden ist KeePassXC und sind die KeePass-Apps für Android und iOS Open Source. Sie haben zwar keine Sicherheitsprüfungen hinter sich, erfreuen sich aber trotzdem großer Beliebtheit und sind damit ständig *im Auge der Öffentlichkeit*.

### Hinweis

**Das KeePass-Format und die KeePass-Apps:**

KeePass an sich ist usrprünglich ein Passwortmanager für Windows **und** ein verschlüsseltes Speicherformat für Passwörter. Daher muss man zwischen **KeePass-Apps** und dem **KeePass-Speicherformat** unterscheiden (letzteres wird auch KeePass-Datenbankformat genannt).

**Das KeePass-Speicherformat** ist ein bestimmtes Format um Passwörter zu speichern, ähnlich wie Word-Dateien ein Format sind, um Texte zu speichern oder wie PowerPoint-Dateien ein Format zum speichern von Präsentationen sind. 

**KeePass-Apps** sind Programme, die das KeePass-Speicherformat zum Speichern von Passwörtern verwenden. Sofern das KeePass-Speicherformat untersützt wird, ist es dabei egal, welche der KeePass-Apps verwendet wird. Die *Orginial-App* nennt sich einfach nur KeePass, ist aber auch nur für Windows erhältlich

Wir werden hier die KeePass-Apps KeePassXC (Windows, MacOS, Linux), KeePass2Android (Android) und Strongbox (iOS) thematisieren. Allesamt nutzen das KeePass-Speicherformat und sind damit kompatibel zueinander - der Name ist zwar unterschiedlich, aber man kann sie wie ein Produkt verwenden.

### Auf einen Blick - Vor- und Nachteile von KeePassXC

**Vorteile:**

* **Ende-zu-Ende-verschlüsselt:** Die Zugangsdaten können nur von Dir, wenn das Masterpasswort bekannt ist, eingesehen werden.
* **Quelloffen:** Der Bauplan von KeePassXC ist einsehbar und kann von Expert:innen überprüft werden.
* **Für alle gängigen Plattformen verfügbar:** Es gibt Programme für Windows, MacOS, Linux, iOS und Android, die das KeePass-Format unterstützen.
* **Autofill im Browser:** Durch Addons integriert sich KeePassXC nahtlos in der Internet-Browser Deiner Wahl.
* **Keine Cloud-Anbindung:** Selbst die verschlüsselten Daten verlassen niemals Dein eigenes Gerät, wenn Du das nicht möchtest.
* **Speicherung in einer Datei:** Die Zugangsdaten werden einfach in einer verschlüsselten, kleinen Datei gespeichert - Du bist also völlig frei, was den Speicherort angeht und kannst sie auch auf einem USB-Stick an Deinem Schlüsselbund speichern.
* **Community-Projekt:** Rund um das KeePass-Format entwickeln eine vielzahl von Freiwilligen KeePassXC und andere KeePass-kompatible Programme. Durch dieses Ökosystem kannst Du einfach zu einem anderen KeePass-Passwortmanager wechseln, wenn das vonnöten sein sollte.


**Nachteile:**

* **Sicherheitsaudits:** KeePassXC hat keine externen Sicherheitsaudits durchlaufen. Das heißt nicht, dass es unsicher sein muss, es wurde aber nicht von Dritten einmal vollumfänglich auf erz und Nieren geprüft.
* **Community-Projekt:** Es gibt keine einheitliche KeePass-kompatible-App für alle Plattformen. KeePassXC gibt es für Windows, MacOS und Linux und für Android und iOS gibt es dafür KeePass2Android und Strongbox. Das ist Vor- und Nachteil zugleich.
* **Keine Cloud-Anbindung:** Möchte man die Zugangsdaten zwischen verschiedenen Geräten synchroniseren, muss man das aufgrund fehlender Cloud-Synchronisierung manuell machen. Auch fehlen Funktionen wie das Teilen von Zugangsdaten.

### Einrichtung KeePassXC

Die Einrichtung funktioniert recht unkompliziert, die passenden [Download-Links finden sich direkt bei KeePassXC](https://keepassxc.org/download/). Lade Dir dort die passende Option herunter und klicke Dich durch den Setup-Prozess. Sollest Du Dir unsicher über die Einrichtung sein, kannst Du nachfolgend zu Details zur passenden Plattform springen. Ansonsten kannst Du mit der Überschrift [Verwendung](#verwendung-1) fortfahren.


**Desktop-PC:**

- [Windows-Installation](#windows-1)
- [MacOS-Installation](#macos-1)
- [Linux-Installation](#linux-1)

**Internet-Browser:**

- [Addon für Mozilla Firefox](#mozilla-firefox-1)
- [Addon für Google Chrome](#google-chrome-1)
- [Addon für Microsoft Edge](microsoft-edge-1)

**Mobilgerät:**

- [Android-Installation](#android-1)
- [iPhone/iOS-Installation](#ios-1)

#### Installation unter Windows, MacOS und Linux

Für den Desktop kann [KeePassXC](https://keepassxc.org) von deren Webseite im [Download-Bereich](https://keepassxc.org/download/) heruntergeladen werden. Das funktioniert für KeePassXC prinzipiell wie für jedes andere Programm auch, die genauen Schritte werden nachfolgend beschrieben. 

##### Windows

- **[KeePassXC](https://keepassxc.org/download/#windows) herunterladen** und die Datei speichern
- **Downloadverzeichnis öffnen:** Den Windows-Explorer öffnen, z.B. durch Druck der Windows-Taste + E und zu Downloads navigieren
- **KeePassXC-Installer ausführen:** Doppelklick auf die eben heruntergeladene KeePassXC-X.X.X-Win64.msi-Datei machen
- **Setup abschließen:** Nun kann man sich nach den eigenen Bedürfnissen durch den Installer klicken und erhält am Ende KeePassXC schlüsselfertig auf den eignen Desktop
- **KeePassXC starten und die Datenbank anlegen:** Nach dem KeePassXC Start muss man noch die Datenbank anlegen, das die Datei, in der Deine Zugangsdaten verschlüsselt gespeichert werden. Dafür lediglich einen Namen, einen Speicherort und ein Passwort wählen, weiter und fertig - KeePassXC ist einzsatzbereit. Die Datei solltest Du nicht verlieren und regelmäßig sichern, ohne diese verlierst Du den Zugriff auf die im Passwortmanager gespeicherten Zugangsdaten.

{{< bildstrecke sichere_logins_02_keepassxc_installation_windows >}}

##### MacOS

- **[KeePassXC](https://keepassxc.org/download/#macos) herunterladen** und die Datei speichern
- **Downloadverzeichnis öffnen:** Den Finder äffnen und zu Downloads navigieren
- **KeePassXC-Installer ausführen:** Doppelklick auf die eben heruntergeladene KeePassXC-X.X.X-XXXXX.dmg Datei machen
- **Setup abschließen:** Nun kann man sich nach den eigenen Bedürfnissen durch den Installer klicken und erhält am Ende KeePassXC schlüsselfertig auf den eigenen Desktop
- **KeePassXC starten und die Datenbank anlegen:** Nach dem KeePassXC Start muss man noch die Datenbank anlegen, das ist die Datei in der Deine Zugangsdaten verschlüsselt gespeichert werden. Dafür lediglich einen Namen, einen Speicherort und ein Passwort wählen, weiter und fertig - KeePassXC ist einsatzbereit. Die Datei solltest Du nicht verlieren und regelmäßig sichern, ohne diese verlierst Du den Zugriff auf die im Passwortmanager gespeicherten Zugangsdaten.

{{< bildstrecke sichere_logins_02_keepassxc_installation_macos >}}

##### Linux

Für Linux gibt es verschiedene Installationsvarianten, wir empfehlen die offiziellen Pakete Deiner Linux-Version (Distribution) zu wählen und nicht die sogenannten cross-distribution packages. Dazu wie folgt vorgehen:

- **KeePassXC über den Linux-Appstore installieren:** Rufe wie gewohnt den Appstore unter Linux auf, für gewöhnlich ist das das Programm namens "Software". Dort KeePassXC suchen und installieren. Alternativ rufe die [KeePassXC-Downloadseite](https://keepassxc.org/download/#linux) auf, suche unter "distribution-specific packages" Deine Linux-Version (Distribution) und kopiere den dort aufgeführten Befehl in das Terminal und führe ihn mit Enter aus.
- **KeePassXC starten und Datenbank anlegen:** Nach dem KeePassXC Start muss man sich noch die Datenbank anlegen, das ist die Datei in der Deine Zugangsdaten verschlüsselt gespeichert werden. Dafür lediglich einen Namen, einen Speicherort und ein Passwort wählen, weiter und fertig - KeePassXC ist einsatzbereit. Die Datei solltest Du nicht verlieren und regelmäßig sichern, ohne diese verlierst Du den Zugriff auf die im Passwortmanager gespeicherten Zugangsdaten.

{{< bildstrecke sichere_logins_02_keepassxc_installation_linux >}}

#### Einrichtung der KeePassXC Browser-Erweiterung

Mithilfe der Browser-Erweiterung kann KeePassXC automatisch Passwörter für und auf Webseiten aufüllen, erstellen und speichern. Das ist ungemein praktisch - einmal eingerichtet und verstanden, muss man nie wieder manuell Passwörter beim Online-Shopping, E-Mails schreiben und so weiter eingeben.

##### Mozilla Firefox

**Hinweis:** Das KeePassXC-Addon greift nicht gesondert auf Deine KeePassXC-Passwortdatei zu, sondern stellt eine Verbindung zu dem KeePassXC-Programm auf Deinem PC her. Das Addon funktioniert daher nur, wenn KeePassXC im Hintergrund offen und entsperrt ist. Die Einrichtung ist nachfolgend im Details beschrieben.

- **[Zum Mozilla AddOn Store navigieren](https://addons.mozilla.org/de/firefox/addon/keepassxc-browser/)** und auf "Zu Firefox hinzufügen" klicken
- In _KeePassXC -> Werkzeuge -> Einstellungen_ zu _Browser-Integration_ navigieren
- Dort den Haken bei _Browserintegration aktivieren_ und _Firefox_ setzen, mit _Ok_ bestätigen
- Anschließend das KeePassXC-Addon im Firefox neuladen
- Und schlussendlich über "_Verbinden_" eine Verbindung zum KeePassXC-Programm auf Deinem PC herstellen

{{< bildstrecke sichere_logins_02_keepassxc_addon_firefox >}}

##### Google Chrome

- **[Zum Chrome Webstore navigieren](https://chrome.google.com/webstore/detail/keepassxc-browser/oboonakemofpalcgghocfoadofidjkkk)** und auf "Hinzufügen" klicken
- In _KeePAssXC -> Werkzeuge -> Einstellungen_ zu _Browser-Integration_ navigieren
- Dort den Haken bei _Browserintegration aktivieren_ und _Chrome, Vivaldi und Brave_ setzen, mit _Ok_ bestätigen
- Anschließend das KeePassXC-Addon im Chrome neuladen
- Und schlussendlich über "_Verbinden_" eine Verbindung zum KeePassXC-Programm auf Deinem PC herstellen

{{< bildstrecke sichere_logins_02_keepassxc_addon_chrome >}}

##### Microsoft Edge

- [Zum Microsoft Edge AddOn Store navigieren](#) und auf "Abrufen" klicken
- In _KeePassXC -> Werkezuge -> Einstellungen_ zu _Browser-Integration_ navigieren
- Dort den Haken bei _Browserintegration aktivieren_ und _Edge_ setzen, mit _Ok_ bestätigen
- Anschließend das KeePassXC-Addon im Chrome neuladen
- Und schlussendlich über "_Verbinden_" eine Verbindung zum KeePassXC-Programm auf Deinem PC herstellen

{{< bildstrecke sichere_logins_02_keepassxc_addon_edge >}}

#### Installation unter Android und iOS

##### Android 

1. [Zum PlayStore navigieren](https://play.google.com/store/apps/details?id=keepass2android.keepass2android&hl=de&gl=US) und "Installieren" wählen
2. KeePass2Android öffnen
3. Eine neue Datenbank anlegen, Speicherort und Masterpasswort wählen
4. Die Datenbank entsperren - fertig!

{{< bildstrecke sichere_logins_02_keepass2android_installation_android >}}

**Hinweis:** KeePass2Android ist leider nicht über den F-Droid Store verfügbar, kann jedoch alternativ direkt als Installationspaket über [GitHub](https://github.com/PhilippC/keepass2android/releases/latest) heruntergeladen werden. Aktualisierungen müssen dann aber manuell getätigt werden. Wer die umfassenden Anbindungsmöglichkeiten externer (Netzwerk-)Speicher von KeePass2Android nicht benötigt oder dafür ohnehin Drittapps verwendet, kann alternativ auch [KeePassDX](https://www.keepassdx.com/) verwenden, welches [im F-Droid Store zu Verfügung](https://f-droid.org/en/packages/com.kunzisoft.keepass.libre/) steht.

##### iOS

1. [Zum AppStore navigieren](#) und "Laden" wählen
2. Installation bestätigen und abwarten
3. Strongbox öffnen und Einrichtungsassistenten durchklicken
	- Wahlweise die Datenbank in der iCloud sichern
	- Autofill in iOS aktivieren
		* iOS-Einstellungen öffnen
		* Zu "Passwörter" navigieren und dort "Automatisch ausfüllen" auswählen
		* Automatisch ausfüllen allgemein und für Strongbox aktivieren
4. Datenbank unter "Loslegen" erstellen
5. Datenbank öffnen und konfigurieren
	- Automatisches Ausfüllen für die Datenbank verwenden
	- Datenbank als Schnellstart verwenden und so als Standard festlegen
	- Alle 2 Wochen an Backups erinnern

{{< bildstrecke sichere_logins_02_strongbox_installation_ios >}} 

### Verwendung

Nun, da KeePassXC installiert und eingerichtet ist, können wir es verwenden. Nachfolgend kannst Du direkt zu bestimmten Themenpunkten springen, wenn Du nicht für alles eine Beschreibung benötigst.

- [Entsperren & die Passwortdatenbank](#entsperren-die-passwortdatenbank-1)
- [Anlegen von Einträgen](#anlegen-von-einträgen-1)
- [Verwalten von Einträgen](#verwalten-von-einträgen-1)
- [Die Web-Browser-Erweiterung](#die-web-browser-erweiterung-1)
- [Backups](#backups-1)

#### Entsperren & die Passwortdatenbank

Der erste Schritt, der bei jedem Start von KeePassXC nötig ist, ist das Entsperren der Passwortdatenbank.

Die Passwortdatenbank ist wie bereits geschrieben ein **verschlüsselter** Speicherbereich bzw. Datenablage in der die Zugangsdaten gespeichert werden können. Im Grunde kann man es sich wie einen Aktenorder vorstellen, den man in einem Tresor ablegt. Es ist die vertrauenswürdige Ablage für all Deine Passwörter und Zugangsinformationen. Daher ist besonders wichtig, diese mit einem möglichst starken Kennwort zu schützen. Dieser Schritt ist bereits bei der Einrichtung gegangen worden, beim Anlegen der KeePassXC-Datenbank nach dem ersten Start. Nun kann die Passwortdatenbank bei jedem Start von KeePassXC mit Deinem Masterpasswort entsperrt werden:

![KeePassXC Datenbank entsperren](/img/sichere_logins_02/keepassxc_db_entsperren.png)

Danach landest Du auch schon direkt in der entsperrten Passwortdatenbank, in Deinem sicheren Ablagebereich für die Zugangsdaten und kannst mit der Verwendung loslegen. Anfangs wird es noch keine Einträge geben, mit der Zeit wird es sich aber hoffentlich füllen:

![KeePassXC Hauptfenster leer](/img/sichere_logins_02/keepassxc_hauptfenster.png)

#### Anlegen von Einträgen

Um einen neuen Eintrag anzulegen, findest Du oben in der Symbolleiste ein kreisförmiges Plus-Symbol. Das ist der Knopf um einen neuen Eintrag zu erstellen. Nach Druck dieses Knopfes kannst Du in der Eingabemaske Die Zugangsdaten für eines Deiner Konten eintragen:

![KeePassXC neuer Eintrag Eingabemaske ausgefüllt mit Beispieldaten](/img/sichere_logins_02/keepassxc_eintrag_erstellen_ausgefüllt_e.png)

1. Der Name des Eintrags. So kannst Du die Einträge später suchen, wiederfinden und zuordnen.
2. Der Benutzer:innenname für das Konto also der Loginname, den Du zum Anmelden verwendest.
3. Schließlich das Passwort für das Konto.
4. Und die URL / Web-Adresse für das Konto. Das ist wichtig, damit KeePassXC in Deinem Browser, z.B. Firefox, automatisch Dein Passwort eintragen kann.
5. Sollte es ein neues Konto sein, kannst Du in KeePassXC direkt ein sicheres Passwort generieren lassen. Das geht über das kleine Würfelsymbol neben dem Passwort-Feld beim Anlegen eines Eintrages.
6. Mit Klick auf "Ok" wird der Eintrag schlussendlich gespeichert.

#### Verwalten von Einträgen

Mit der Zeit haben sich dann hoffentlich einige Einträge in Deinem KeePassXC Passwortmanager gesammelt:

![KeePassXC Hautpfenster ausgefüllt mit Beispieleinträgen](/img/sichere_logins_02/keepassxc_haupfenster_ausgefüllt_e.png)

1. Das Plus-Symbol erlaubt Dir, immer weitere Einträge und Zugangsdaten zu speichern.
2. Im oberen Bereich kannst Du durch alle Einträge blättern.
3. Im unteren Bereich kannst Du die Details zum aktuell angewählten Bereich betrachten.
4. Mit einem Doppelklick kannst Du einen Eintrag bearbeiten und mit einem Rechtsklick weitere Aktionen ausführen.

#### Die Web-Browser-Erweiterung

Besonders praktisch an KeePassXC ist auch die Web-Browser-Erweiterung. Das ist ein kleines Zusatzprogramm für Deinen Web-Browser, zum Beispiel für Firefox. Die Installation haben wir ebenfalls schon am Anfang dieses Artikels behandelt.

Das Firefox-Addon steht Dir als kleines Symbol oben rechts in Deinem Firefox zur Verfügung. In Google Chrome und anderen Web-Browsern solltest Du auch ein Icon an ähnlicher Stelle finden. Daher werden wir hier nur Bildschirmfotos für Firefox einfügen. Um es zu Verwenden, musst Du KeePassXC im Hintergrund gestartet und entsperrt haben, wenn alles ist, wie es sein soll, sieht es dann wie folgt aus:

![KeePassXC Firefox Addon Hauptfenster Status verbunden](/img/sichere_logins_02/keepassxc_firefox_connected.png)

Soweit nichts besonderes. Das praktische am Addon ist die Autofill-Funktion.

Die Autofill-Funktion ermöglicht es, dass Deine Passwörter bzw. Zugangsdaten auf Webseiten automatisch ausgefüllt werden und Du sie somit nicht kopieren oder händisch eingeben. Dazu musst Du einfach die Webseite aufrufen. KeePassXC sucht automatisch nach passenden Einträgen und wenn es welche findet, musst Du den Zugriff darauf genehmigen - wenn Du auf "Merken" klickst, ist das einmalig (hier am Beispiel der GLS-Webseite):

![KeePassXC Autofill Bestätigen Dialog](/img/sichere_logins_02/keepassxc_firefox_zugriffsanfrage_e.png)

Künftig musst Du nur in das Anmeldefeld klicken und Dir wird dann automatisch der passender Eintrag vorgeschlagen und nach Klick auf diesen werden die Zugangsdaten auf der Webseite augefüllt:

![KeePassXC Firefox Addon Logindaten-Auswahl auf der GLS Webseite](/img/sichere_logins_02/keepassxc_firefox_loginauswahl_e.png)

![KeePassXC Firefox Logindaten ausgefüllt nach Auswahl im Addon auf der GLS-Webseite](/img/sichere_logins_02/keepassxc_firefox_login_ausgefüllt_e.png)

#### Backups

Da bei KeePassXC Deine Zugänge einfach in einer verschlüsselten Datei gespeichert werden, kannst Du diese stinknormal, wie Deine bisherigen Dateien auch, backuppen. Das solltest Du auch in jedem Fall machen, denn wenn Du die KeePassXC-Datei verlierst oder diese beschädigt wird, hast Du den Zugriff auf Deine Passwörter verloren!! 

## Literatur und Quellen

- [KeePassXC Webseite](https://keepassxc.org)
- [Bitwarden Webseite](https://bitwarden.com)
