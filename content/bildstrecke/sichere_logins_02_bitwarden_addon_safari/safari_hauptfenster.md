---
title: Bitwarden im Safari aktivieren 
description: Wenn Du das Bitwarden-Programm auf Deinem Macbook installiert hast, wird automatisch auch die Bitwarden-Erweiterung für Safari installiert. Diese muss lediglich noch aktiviert werden. Navigiere dafür einfach in die Safari-Einstellungen, in der Menüleiste unter "Safari" → "Einstellungen".
image: /img/sichere_logins_02/01_safari_startseite.png
weight: 1
---
