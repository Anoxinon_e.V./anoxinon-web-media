---
title: Bitwarden im AppStore suchen 
description: Als erstes musst Du den AppStore öffnen, dieser findet sich als Symbol auf Deinem iPhone-Homescreen. Suche ihn und klicke ihn an. 
image: /img/sichere_logins_02/ios_homescreen_e_appstore.png
weight: 1
---
