---
title: GPG4Win herunterladen 
description: Als erstes musst du GPG4Win herunterladen. Öffne dazu einen Browser deiner Wahl, in unserem Fall Firefox.
image: /img/gpg4win_gpa_install/01_windows_11_desktop_firefox_e.png
weight: 10
---