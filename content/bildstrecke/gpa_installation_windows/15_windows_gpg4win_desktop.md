---
title: GPA ist installiert
description: Du findest GPA ab sofort auf dem Desktop und kannst es verwenden.
image: /img/gpg4win_gpa_install/15_windows_gpg4win_desktop_e.png
weight: 150
---