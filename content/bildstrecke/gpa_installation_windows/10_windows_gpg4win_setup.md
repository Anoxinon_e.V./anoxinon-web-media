---
title: GPG4Win Installer durchlaufen
description: Nun begrüßt dich der Installer und ist bereit, zu beginnen. Bestätige dies.
image: /img/gpg4win_gpa_install/10_windows_gpg4win_setup_e.png
weight: 100
---