---
title: GPG4Win Installer durchlaufen
description: An dieser Stelle kannst du den Installationsort ändern. Die Voreinstellung passt wunderbar, klicke dich nur weiter.
image: /img/gpg4win_gpa_install/12_windows_gpg4win_installdir_e.png
weight: 120
---