---
title: GPG4Win herunterladen 
description: Die Entwicklende bieten dir direkt beim Download die Möglichkeit zu spenden. Möchtest du das nicht, wähle $0 und du gelangst direkt zum Download. Wenn du zufrieden bist, kannst du jederzeit zurückkehren und spenden - die Entwickelnden danken es dir.
image: /img/gpg4win_gpa_install/04_windows_gpg4win_downloadseite_e.png
weight: 40
---