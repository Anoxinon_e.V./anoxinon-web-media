---
title: GPG4Win Installer vorbereiten
description: Anschließend kannst du die Sprache auswählen, die später in GPA eingestellt sein wird.
image: /img/gpg4win_gpa_install/09_windows_gpg4win_lang_e.png
weight: 90
---