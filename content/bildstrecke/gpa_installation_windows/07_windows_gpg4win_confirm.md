---
title: GPG4Win Installer vorbereiten
description: Nun benötigt der GPG4Win Installer Berechtigungen, um alle notwendigen Aktionen durchführen zu können. Bestätige dies.
image: /img/gpg4win_gpa_install/07_windows_gpg4win_confirm_e.png
weight: 70
---