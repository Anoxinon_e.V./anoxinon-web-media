---
title: GPG4Win Installer durchlaufen
description: An dieser Stelle kannst du auswählen, welche Komponenten installiert werden sollen. Zu Wahl stehen verschiedene OpenPGP-Werkzeuge. Für uns wichtig ist GPA, das muss auf jedenfall angehakt werden. In unserem Beispiel entscheiden wir uns außerdem dazu, noch alles weitere zu installieren.
image: /img/gpg4win_gpa_install/11_windows_gpg4win_components_e.png
weight: 110
---