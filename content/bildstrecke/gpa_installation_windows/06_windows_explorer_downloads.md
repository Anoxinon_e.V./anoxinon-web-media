---
title: GPG4Win Installer vorbereiten
description: Öffne nun den Windows Explorer und navigiere zu den Downloads. Dort kannst du durch einen Doppelklick auf den eben heruntergeladenen GPG4Win-Installer die Installation beginnen.
image: /img/gpg4win_gpa_install/06_windows_explorer_downloads_e.png
weight: 60
---