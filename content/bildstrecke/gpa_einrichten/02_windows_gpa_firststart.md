---
title: GPA Schlüsselpaar erstellen
description: Wenn du GPA neu installiert und noch keinen OpenPGP-Schlüssel hast, bietet dir das Programm beim ersten Start an, ein neues OpenPGP-Schlüsselpaar zu erstellen. Folge diesem Vorschlag durch einen Klick auf "Jetzt Schlüssel erzeugen". 
image: /img/gpa_setup/02_windows_gpa_firststart_e.png
weight: 20
---