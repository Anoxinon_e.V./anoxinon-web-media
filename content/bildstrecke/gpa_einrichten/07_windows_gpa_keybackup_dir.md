---
title: GPA Sicherungskopie anfertigen
description: Nun kannst du auswählen, wo das Backup deines deines Schlüsselpaares gespeichert werden soll. Wie bereits geschrieben - verwende einen sicheren Ort! 
image: /img/gpa_setup/07_windows_gpa_keybackup_dir_e.png
weight: 70
---