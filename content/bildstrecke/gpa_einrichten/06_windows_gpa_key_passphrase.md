---
title: GPA Schlüsselpaar erstellen
description: Zur Sicherheit wird dein privater Schlüssel mit einem Passwort deiner Wahl gesichert. Dieses musst du folgend angeben.
image: /img/gpa_setup/06_windows_gpa_key_passphrase_e.png
weight: 60
---