---
title: GPA Schlüsselpaar erstellen
description: Nun wirst du nach einem Name des Schlüssel-Besitzenden gefragt. Das solltest du sein, trage also dein Namen ein.
image: /img/gpa_setup/03_windows_gpa_keyowner_name_e.png
weight: 30
---