---
title: GPA Sicherungskopie anfertigen
description: GPA gibt dir Bescheid, dass die Sicherungskopie abgeschlossen ist und zeigt dir nochmals den Speicherort. Verwahre ihn sicher und vergiss ihn nicht! Du braucht es, wenn dein PC defekt ist, sonst hast du keinen Zugriff mehr auf verschlüsselte Inhalte.
image: /img/gpa_setup/09_windows_gpa_keybackup_completed_e.png
weight: 90
---