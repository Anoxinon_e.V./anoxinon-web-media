---
title: GPA Schlüsselpaar erstellen
description: Nun schlägt dir GPA vor, eine Sicherhungskopie des Schlüssels zu speichern. Das solltest du unbedingt tun! Verwende hier einen Speicherort auf den nur du Zugriff hast.
image: /img/gpa_setup/05_windows_gpa_keybackup_e.png
weight: 50
---