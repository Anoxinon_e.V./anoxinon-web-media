---
title: GPA Schlüsselpaar erstellen
description: Anschließend musst du die (primäre) E-Mail Adresse angeben, für die dieser Schlüssel gedacht ist.
image: /img/gpa_setup/04_windows_gpa_key_e-mail_addr_e.png
weight: 40
---