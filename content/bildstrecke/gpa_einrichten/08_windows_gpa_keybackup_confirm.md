---
title: GPA Sicherungskopie anfertigen
description: Um das Schlüsselpaar zu sichern, benötigst du das Passwort, mit dem du deinen privaten Schlüssel gesichert hast. Das ist das Passwort, was du eben ausgewählt hast.
image: /img/gpa_setup/08_windows_gpa_keybackup_confirm_e.png
weight: 80
---