---
title: KeePassXC installieren 
description: Es erscheint der Hinweis, dass KeePassXC eine ausführbare Datei ist. Das kann so bestätigt werden, denn das soll so - schließlich wollen wir KeePassXC installieren.
image: /img/sichere_logins_02/05_win_keepassxc_confirm_installer_e.png
weight: 5
---
