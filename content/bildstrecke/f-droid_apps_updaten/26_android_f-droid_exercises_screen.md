---
title: Aktualisierungen einsehen
description: Im Tab "Aufgaben" sind die verfügbaren Aktualisierungen aufgelistet. Über die Schaltfläche "Aktualisierungen" können die Updates angestoßen werden.
image: /img/f-droid_als_app_bezugsquelle/26_android_f-droid_exercises_screen_e.png
weight: 26
---
