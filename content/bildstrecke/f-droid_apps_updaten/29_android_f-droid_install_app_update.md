---
title: Aktualisierungen durchführen
description: Schlussendlich lässt sich die App über die Schaltfläche "Installieren" im aufploppenden Dialog updaten. Fertig!
image: /img/f-droid_als_app_bezugsquelle/29_android_f-droid_install_app_update_e.png
weight: 29
---
