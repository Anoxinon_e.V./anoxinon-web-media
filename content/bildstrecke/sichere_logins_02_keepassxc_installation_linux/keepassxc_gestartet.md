---
title: KeePassXC erster Start
description: Starte nun KeePassXC, dann kannst Du loslegen - Um Passwörter zu speichern, musst Du eine Passwortdatei erstellen, bei KeePassXC "Datenbank" genannt. Dazu auf "Neue Datenbank erstellen" klicken.
image: /img/sichere_logins_02/keepassxc_willkommensfenster_e.png
weight: 6 
---
