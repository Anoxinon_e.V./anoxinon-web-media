---
title: KeePassXC einrichten 
description: Um Deine Passwortdatei zu speichern, braucht KeePassXC Zugriff auf Deine Dateien. Erlaube das. 
image: /img/sichere_logins_02/15_macos_keepassxc_access_desktop.png
weight: 16
---
