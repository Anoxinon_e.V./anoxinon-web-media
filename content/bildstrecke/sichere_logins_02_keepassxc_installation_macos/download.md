---
title: KeePassXC herunterladen 
description: Man wird nun vor die Wahl einer Intel- und einer Silicon-Version gestellt. Mit letzterem sind die neuen MacBooks mit M1-Chip gemeint. Hier bist Du am Zug, das für Dich passende zu wählen.
image: /img/sichere_logins_02/02_macos_keepassxc_webseite_download.png
weight: 2
---
