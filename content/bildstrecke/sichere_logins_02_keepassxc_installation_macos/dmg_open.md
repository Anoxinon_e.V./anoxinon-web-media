---
title: KeePassXC installieren 
description: Öffne die heruntergeladene Datei, sie enthält das KeePassXC-Programm.
image: /img/sichere_logins_02/03_macos_keepassxc_dmg_open.png
weight: 3
---
