---
title: KeePassXC einrichten 
description: An der Stelle kannst Du erweiterte Einstellungen zur Verschlüsselung konfigurieren. Belasse das beim Standard, sofern Du keinen speziellen Grund für Änderungen hast. 
image: /img/sichere_logins_02/11_macos_keepassxc_createdb_settings.png
weight: 11
---
