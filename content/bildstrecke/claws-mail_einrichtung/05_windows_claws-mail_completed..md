---
title: Claws Mail einrichten
description: "Schlussendlich ist die Einrichtung abgeschlossen und du kannst sie mit Druck auf Speichern abspeichern."
image: /img/claws-mail_setup/05_windows_claws-mail_completed_e.png
weight: 60
---