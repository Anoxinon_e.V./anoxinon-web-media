---
title: Claws Mail einrichten
description: "Nun kannst du deinen Namen und E-Mail Adresse anlegen, die später für E-Mails verwendet wird."
image: /img/claws-mail_setup/02_windows_claws-mail_e-mail_addr_e.png
weight: 30
---