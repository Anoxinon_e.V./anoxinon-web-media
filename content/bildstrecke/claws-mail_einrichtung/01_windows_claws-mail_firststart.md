---
title: Claws Mail einrichten
description: "Claws Mail begrüßt dich mit einem Willkommensfenster. Mit Klick auf die Schaltfläche Weiter / Next beginnt die Ersteinrichtung."
image: /img/claws-mail_setup/00_windows_claws-mail_desktop_e.png
weight: 20
---