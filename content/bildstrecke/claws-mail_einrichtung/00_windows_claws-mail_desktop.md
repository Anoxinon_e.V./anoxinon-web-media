---
title: Claws Mail einrichten
description: "Starte Claws Mail zum ersten Mal. In unserem Beispiel doppelklicken wir dafür den Starter auf dem Windows Desktop."
image: /img/claws-mail_setup/00_windows_claws-mail_desktop_e.png
weight: 10
---