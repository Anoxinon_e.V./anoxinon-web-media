---
title: Claws Mail einrichten
description: "Folgend musst du die Daten zum Empfangen von E-Mails eingeben. Mittels der Schaltfläche Automatisch konfigurieren lassen sich die Daten bei den meisten Anbietenden automatisch abrufen. Achte unbedingt darauf, dass die Transportverschlüsselung und ggfs STARTTLS aktiviert wird."
image: /img/claws-mail_setup/03_windows_claws-mail_receive_e.png
weight: 40
---