---
title: Claws Mail einrichten
description: "Nun musst du die Daten zum Senden von E-Mails angeben. Diese stellt dein E-Mail Anbietender bereit. Achte auch hier unbedingt darauf, dass die Transportverschlüsselung und ggfs STARTTLS aktiviert ist."
image: /img/claws-mail_setup/04_windows_claws-mail_send_e.png
weight: 50
---