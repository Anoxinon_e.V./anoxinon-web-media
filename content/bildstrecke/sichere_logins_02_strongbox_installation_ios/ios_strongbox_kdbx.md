---
title: Strongbox Passwortdatei anlegen
description: Nun können wir eine Passwortdatei hinzufügen. Da wir noch keine haben, wählen wir an der Stelle "Neue Datenbank erstellen" um die Passwortdatei zu kreieren. 
image: /img/sichere_logins_02/ios_strongbox_firststart_12_e.PNG
weight: 22
---
