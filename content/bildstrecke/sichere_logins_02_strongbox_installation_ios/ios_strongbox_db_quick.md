---
title: Strongbox entsperren 
description: Strongbox kann mit mehreren Passwortdateien umgehen. Hier kannst Du auswählen, dass die eben von Dir erstellte standardmäßig geöffnet und verwendet werden soll. Das kannst Du aktivieren.
image: /img/sichere_logins_02/ios_strongbox_firststart_19_e.PNG
weight: 29
---
