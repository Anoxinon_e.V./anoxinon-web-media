---
title: Strongbox einrichten  
description: Schalte hier den Toggle für Autofill an und wähle Strongbox als AutoFill-App aus. Am Ende sollte es wie im Bildschirmfoto zu sehen aussehen. 
image: /img/sichere_logins_02/ios_strongbox_firststart_08_e.PNG
weight: 17
---
