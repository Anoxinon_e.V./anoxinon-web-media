---
title: Strongbox Passwortdatei ist angelegt 
description: Fertig ist die Passwortdatei, nun kannst Du sie öffnen! Wähle dazu "Datenbank jetzt öffnen". 
image: /img/sichere_logins_02/ios_strongbox_firststart_15_e.PNG
weight: 25
---
