---
title: Strongbox entsperren 
description: Anschließend kannst Du die Passwortdatei mit dem eben gewählten Masterpasswort entsperren. Trage es dazu in das Formularfeld ein und wähle "Entsperren".
image: /img/sichere_logins_02/ios_strongbox_firststart_16_e.PNG
weight: 26
---
