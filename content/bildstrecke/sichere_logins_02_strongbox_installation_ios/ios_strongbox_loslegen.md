---
title: Strongbox Passwortdatei anlegen
description: Noch können wir keine Passwörter speichern, dazu brauchen wir erst eine KeePass-Passwortdatei. Um eine anzulegen, tippe auf "Loslegen"
image: /img/sichere_logins_02/ios_strongbox_firststart_11_e.PNG
weight: 21
---
