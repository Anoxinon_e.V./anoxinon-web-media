---
title: Bitwarden-Addon ist installiert 
description: Sobald die Installation abgeschlossen ist, öffnet sich ein automatisch eine Webseite von Bitwarden. Wenn Du möchtest, ließ sie Dir durch, ansonsten kannst Du sie einfach schließen.
image: /img/sichere_logins_02/edge_bitwarden_08_e.png
weight: 8
---
