---
title: Bitwarden für Edge suchen 
description: Dort kann man über "Erweiterungen für Microsoft Edge abrufen" neue Addons suchen und installieren.
image: /img/sichere_logins_02/edge_bitwarden_02_e.png
weight: 2
---
