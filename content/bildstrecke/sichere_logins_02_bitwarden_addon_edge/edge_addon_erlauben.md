---
title: Bitwarden für Edge installieren 
description: Edge macht Dich noch auf die Berechtigungen, die Bitwarden benötigt, aufmerksam. Nach einem weiteren Klick auf "Erweiterung hinzufügen" ist das Addon installiert.
image: /img/sichere_logins_02/edge_bitwarden_06_e.png
weight: 6
---
