---
title: App installieren
description: Nun kann man über die Schaltfläche "Installieren" die Installation anstoßen.
image: /img/f-droid_als_app_bezugsquelle/23_android_f-droid_app_install_e.png
weight: 23
---
