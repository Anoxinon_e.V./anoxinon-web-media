---
title: F-Droid Berechtigung vergeben
description: Man sieht, das F-Droid als nicht vetrauenswürdige Quelle eingestellt ist. Dies muss man über den Schalter ändern.
image: /img/f-droid_als_app_bezugsquelle/21_android_f-droid_untrusted_source_e.png
weight: 21
---
