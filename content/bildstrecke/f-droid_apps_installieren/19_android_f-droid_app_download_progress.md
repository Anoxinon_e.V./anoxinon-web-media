---
title: App herunterladen
description: Das Herunterladen der Applikation kann einige Sekunden dauern.
image: /img/f-droid_als_app_bezugsquelle/19_android_f-droid_app_download_progress.png
weight: 19
---
