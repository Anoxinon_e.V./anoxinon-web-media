---
title: F-Droid Berechtigung vergeben
description: Bei der ersten Installatio muss einmalig die Berechtigung zum Installieren von Apps an F-Droid vergeben werden. Dazu dem Dialog folgen und die Einstellungen öffnen.
image: /img/f-droid_als_app_bezugsquelle/20_android_f-droid_no_install_permissions_e.png
weight: 20
---
