---
title: App wird installiert
description: Die Installation kann einige Sekunden dauern.
image: /img/f-droid_als_app_bezugsquelle/24_android_f-droid_app_install_progress.png
weight: 24
---
