---
title: F-Droid Berechtigung vergeben
description: Wenn man den Schalter eingeschaltet hat und so F-Droid als vertrauenswürdige Quelle hinterlegt hat, kann man zurückgehen und mit der Installation der App forfahren.
image: /img/f-droid_als_app_bezugsquelle/22_android_f-droid_trusted_source_e.png
weight: 22
---
