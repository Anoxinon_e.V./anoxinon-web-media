---
title: KeePass2Android ist installiert 
description: Nachdem KeePass2Android installiert ist, kannst Du es öffnen um die Einrichtung abzuschließen. 
image: /img/sichere_logins_02/06_android_keepass2android_e.PNG
weight: 6
---
