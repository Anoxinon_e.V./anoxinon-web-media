---
title: Bitwarden für Firefox suchen 
description: In der Addon-Verwaltung gibt es ein Suchfeld, in dem Du den riesigen Addon-Katalog von Mozilla Firefox durchsuchen kannst. In unserem Fall gibst Du hier "Bitwarden" ein und drückst Enter.
image: /img/sichere_logins_02/firefox_addons_empty_e.png
weight: 2
---
