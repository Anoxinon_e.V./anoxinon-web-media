---
title: Bitwarden für Firefox installieren 
description: Firefox macht Dich noch auf die Berechtigungen, die Bitwarden benötigt, aufmerksam. Nach einem weiteren Klick auf "Hinzufügen" ist das Addon installiert.
image: /img/sichere_logins_02/mozilla_addons_bitwarden_install_e.png
weight: 5
---
