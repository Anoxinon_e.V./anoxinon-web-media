---
title: Bitwarden für Firefox suchen 
description: Im Firefox-Menü gibt es den Punkt Addons. Mit einem Klick darauf gelangst Du zur Addon-Verwaltung.
image: /img/sichere_logins_02/firefox_menü_addons_e.png
weight: 1
---
