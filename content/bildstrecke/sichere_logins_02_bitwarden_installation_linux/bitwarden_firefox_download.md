---
title: Bitwarden Download 
description: Im Download-Bereich des eigenen Internet-Browsers, in unserem Fall Mozilla Firefox, findet man die heruntergeladene Datei und hat durch Anwählen des Ordnersymbols die Möglichkeit, direkt in den Ordner bzw. an den Speicherort zu navigieren.
image: /img/sichere_logins_02/bitwarden_firefoxdownload_e.png
weight: 3
---
