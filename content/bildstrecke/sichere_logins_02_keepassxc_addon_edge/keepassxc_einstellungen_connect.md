---
title: KeePassXC-Addon mit KeePassXC verbinden 
description: Dort musst Du unter "Browser-Integration" (1) den Haken bei "Browserintegration aktivieren" (2) setzen und dort "Edge" anwählen (3). Danach mit "Ok" (4) abspeichern.
image: /img/sichere_logins_02/edge_keepassxc_09_e.png
weight: 9
---
