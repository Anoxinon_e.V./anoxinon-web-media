---
title: KeePassXC-Addon mit KeePassXC verbinden 
description: Das Addon muss noch verbunden werden. Das muss zuvor noch in KeePasXC erlaubt werden - wechsle dazu in Dein KeePassXC-Programm und klicke unter "Werkzeuge" auf "Einstellungen".
image: /img/sichere_logins_02/edge_keepassxc_08_e.png
weight: 8
---
