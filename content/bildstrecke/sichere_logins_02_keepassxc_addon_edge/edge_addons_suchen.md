---
title: KeePassXC für Edge suchen 
description: Im Suchfeld kannst Du nun den Edge Addon-Store nach KeePassXC durchsuchen, ein Druck auf Enter bringt Dich zu den Suchergebnissen.
image: /img/sichere_logins_02/edge_keepassxc_03_e.png
weight: 3
---
