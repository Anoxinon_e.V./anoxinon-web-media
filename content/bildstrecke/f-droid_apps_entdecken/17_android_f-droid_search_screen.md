---
title: Apps suchen
description: Die App-Suche sieht wie zu sehen aus, dort kann man zum Beispiel mit Hilfe von Stichwörtern nach einem App-Name oder der App-Beschreibung suchen.
image: /img/f-droid_als_app_bezugsquelle/17_android_f-droid_search_screen_e.png
weight: 17
---
