---
title: Bitwarden Installation starten 
description: Für die Installation braucht Bitwarden Admin-Rechte, dafür einfach in dem aufploppenden Dialog mit "Ja" bestätigen.
image: /img/sichere_logins_02/05_win_bitwarden_install_confirm_e.png
weight: 5
---
