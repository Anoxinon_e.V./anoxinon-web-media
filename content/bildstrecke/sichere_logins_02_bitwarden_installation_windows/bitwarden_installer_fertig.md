---
title: Bitwarden Installation abschließen 
description: Nach erfolgreicher Installation meldet sich Bitwarden und man kann das Ganze mit Klick auf "Fertigstellen" abschließen. 
image: /img/sichere_logins_02/08_win_bitwarden_installer_finished_e.png
weight: 8
---
