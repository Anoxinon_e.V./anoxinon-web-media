---
title: Bitwarden Installation starten 
description: Nun kann die Installation mit einem Klick auf "Installieren" gestartet werden. 
image: /img/sichere_logins_02/06_win_bitwarden_installer_e.png
weight: 6
---
