---
title: Bitwarden Download 
description: Nun findet man zentral auf der Webseite die Downloads, in unserem Fall entscheiden wir uns für Windows. Durch Anwählen des Links kann man sich die Installationsdatei herunterladen.
image: /img/sichere_logins_02/02_win_bitwarden_webseite_download_e.png
weight: 2
---
