---
title: F-Droid herunterladen
description: Um F-Droid zu installieren, musst du es zuerst herunterladen. Öffne dazu einen Browser deiner Wahl. Bei den meisten Android-Smartphones ist einer vorinstalliert und auf dem Homescreen zu finden.
image: /img/f-droid_als_app_bezugsquelle/01_android_homescreen_e.jpg
weight: 1
---
