---
title: F-Droid herunterladen
description: Anschließend musst du den Download - je nach Browser - noch bestätigen.
image: /img/f-droid_als_app_bezugsquelle/06_android_f-droid_download_file_e.png
weight: 6
---
