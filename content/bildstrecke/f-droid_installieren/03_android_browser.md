---
title: F-Droid herunterladen
description: F-Droid kannst du auf der F-Droid Webseite herunterladen. Um dorthin zu gelangen öffne die Addresszeile deines Browsers...
image: /img/f-droid_als_app_bezugsquelle/03_android_browser_e.png
weight: 3
---
