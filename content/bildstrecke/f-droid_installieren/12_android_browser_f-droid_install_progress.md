---
title: F-Droid installieren
description: Der Installationsprozess kann einige Sekunden dauern.
image: /img/f-droid_als_app_bezugsquelle/12_android_browser_f-droid_install_progress.png
weight: 12
---
