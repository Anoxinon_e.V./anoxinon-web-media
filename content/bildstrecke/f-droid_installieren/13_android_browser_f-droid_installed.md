---
title: F-Droid ist installiert!
description: Nach Abschluss der Installation kannst du F-Droid direkt öffnen.
image: /img/f-droid_als_app_bezugsquelle/13_android_browser_f-droid_installed_e.png
weight: 13
---
