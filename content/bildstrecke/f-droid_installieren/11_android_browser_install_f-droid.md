---
title: F-Droid installieren
description: Nun kannst du F-Droid durch Klick auf "Installieren" installieren.
image: /img/f-droid_als_app_bezugsquelle/11_android_browser_install_f-droid_e.png
weight: 11
---
