---
title: KeePassXC für Chrome suchen 
description: iIm Webstore gibt es ein Suchfeld, in dem Du den Addon-Katalog von Google Chrome durchsuchen kannst. In unserem Fall gibst Du hier "KeePassXC" ein und drückst Enter"
image: /img/sichere_logins_02/chrome_keepassxc_02_e.png
weight: 2
---
