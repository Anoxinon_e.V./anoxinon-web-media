---
title: KeePassXC für Chrome installieren 
description: Chrome macht Dich noch auf die Berechtigungen, die KeePassXC benötigt aufmerksam. Nach einem weiteren Klick auf "Erweiterung hinzufügen" ist das Addon installiert.
image: /img/sichere_logins_02/chrome_keepassxc_05_e.png
weight: 5
---
