---
title: F-Droid Einstellungen
description: Im Tab "Einstellungen" lässt sich das Verhalten von F-Droid anpassen. Unter anderem lassen sich die Paketquellen, auch F-Droid Repositories genannt, verwalten.
image: /img/f-droid_als_app_bezugsquelle/30_android_f-droid_settings_screen_e.png
weight: 30
---
