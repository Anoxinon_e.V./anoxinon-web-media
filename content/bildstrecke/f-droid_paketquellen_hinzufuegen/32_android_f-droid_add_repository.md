---
title: Neue Paketquelle hinzufügen
description: Durch die Eingabe der URL und optional des Repository-Fingerprints lässt sich eine neue Paketquelle einrichten. Tipp - Wenn du eine Paketquellen-URL in die Zwischenablage kopierst, wird diese hier automatisch ausgefüllt. Manche Browser öffnen sogar F-Droid-Repository-Links automatisch in F-Droid. Damit hat man drei Wege, ein neues Repository hinzuzufügen.
image: /img/f-droid_als_app_bezugsquelle/32_android_f-droid_add_repository.png
weight: 32
---
