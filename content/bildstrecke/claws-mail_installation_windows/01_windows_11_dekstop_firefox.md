---
title: Claws Mail herunterladen
description: Als erstes müssen wir uns die für Claws Mail benötigten Dateien herunterladen. Dazu öffne einen Browser deiner Wahl, in unserem Beispiel Firefox.
image: /img/claws-mail_install_windows/01_windows_11_desktop_firefox_e.png
weight: 10
---