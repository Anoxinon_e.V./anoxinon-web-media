---
title: Microsoft Visual C++ Servicepack herunterladen
description: "Claws Mail benötigt außerdem das Microsoft Visual C++ Servicepack. Du kannst es von Microsoft herunterladen, rufe dazu die folgende Webseite auf: https://www.microsoft.com/de-de/download/details.aspx?id=26999"
image: /img/claws-mail_install_windows/06_microsoft_visual_cpp_servicepack_downloadseite_e.png
weight: 70
---