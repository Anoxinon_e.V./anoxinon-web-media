---
title: Claws Mail ist installiert
description: "Claws Mail meldet die abgeschlossene Installation, beende den Installer also durch Klick auf Weiter."
image: /img/claws-mail_install_windows/22_windows_claws-mail_installed_e.png
weight: 230
---