---
title: Microsoft Visual C++ Servicepack installieren
description: "Microsoft fragt nun nach der Berechtigung zur Installation des Servicepacks. Das kannst du bestätigen."
image: /img/claws-mail_install_windows/10_microsoft_visual_cpp_servicepacke_confirm_e.png
weight: 110
---