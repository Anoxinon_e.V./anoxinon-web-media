---
title: Microsoft Visual C++ Servicepack installieren
description: "Bei Abschluss der Installation meldet Microsoft den Erfolg, mit Klick auf die Schaltfläche Fertigstellen kann man die Installation abschließen."
image: /img/claws-mail_install_windows/12_microsoft_visual_cpp_complete_e.png
weight: 130
---