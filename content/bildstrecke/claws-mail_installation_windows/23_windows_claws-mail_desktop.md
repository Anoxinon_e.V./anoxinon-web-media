---
title: Claws Mail ist installiert
description: "Schlussendlich findet sich Claws Mail auf dem Desktop und ist bereit für die Verwendung."
image: /img/claws-mail_install_windows/23_windows_claws-mail_desktop_e.png
weight: 240
---