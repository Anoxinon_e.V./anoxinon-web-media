---
title: Claws Mail installieren
description: "Folgend kannst du wählen, in welchen Ordner des Startmenüs der Claws Mail Starter gelegt wird. Der Standard ist okay, gehe weiter."
image: /img/claws-mail_install_windows/20_windows_claws-mail_startmenu_dir_e.png
weight: 210
---