---
title: Microsoft Visual C++ Servicepack herunterladen
description: "Man hat die Wahl zwischen verschiedenen Varianten, wenn du einen halbwegs aktuellen handelsüblichen PC hast, ist die dritte Variante (x64) korrekt."
image: /img/claws-mail_install_windows/07_microsoft_visual_cpp_servicepack_choice_downloadseite_e.png
weight: 80
---