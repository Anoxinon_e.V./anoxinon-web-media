---
title: Claws Mail installieren
description: "Hier kannst du einstellen, ob Claws Mail als Standardprogramm für E-Mail-Adresslinks hinterlegt werden soll. Standardmäßig ist das schon an, das passt wunderbar so."
image: /img/claws-mail_install_windows/19_windows_claws-mail_uri_register_e.png
weight: 200
---