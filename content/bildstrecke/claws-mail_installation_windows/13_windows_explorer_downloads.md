---
title: Claws Mail installieren  
description: "Nun ist alles bereit für die Installation von Claws Mail. Navigiere also wieder in deine Downloads im Windows Explorer und starte den zuvor heruntergeladenen Claws Mail Installer via Doppelklick."
image: /img/claws-mail_install_windows/13_windows_explorer_downloads_e.png
weight: 140
---