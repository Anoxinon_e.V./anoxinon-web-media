---
title: Microsoft Visual C++ Servicepack installieren
description: "Nach dem Download kannst du das Microsoft Visual C++ Servicepack installieren. Öffne dazu den Windows Explorer und das Downloadverzeichnis und Beginne mit einem Doppelklick auf die entsprechende Datei."
image: /img/claws-mail_install_windows/09_windows_explorer_downloads_e.png
weight: 100
---