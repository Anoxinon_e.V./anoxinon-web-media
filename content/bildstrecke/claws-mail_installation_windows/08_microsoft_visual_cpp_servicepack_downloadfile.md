---
title: Microsoft Visual C++ Servicepack herunterladen
description: "Anschließend wird dir die Datei zum Herunterladen angeboten."
image: /img/claws-mail_install_windows/08_microsoft_visual_cpp_servicepack_downloadfile_e.png
weight: 90
---