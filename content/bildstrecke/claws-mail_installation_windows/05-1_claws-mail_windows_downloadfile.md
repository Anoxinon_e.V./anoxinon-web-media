---
title: Claws Mail herunterladen
description: Anschliend wird dir die Datei zum Herunterladen angeboten.
image: /img/claws-mail_install_windows/05-1_claws-mail_windows_downloadfile_e.png
weight: 60
---