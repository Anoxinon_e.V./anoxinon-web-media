---
title: Claws Mail herunterladen
description: Dort kannst du Claws Mail herunterladen. Achte darauf, die Version 4 herunterzuladen. Wir wählen außerdem die 64-Bit Variante, jedes Gerät der letzten paar Jahre sollte damit klarkommen.
image: /img/claws-mail_install_windows/05-0_claws-mail_windows_downloadseite_e.png
weight: 50
---