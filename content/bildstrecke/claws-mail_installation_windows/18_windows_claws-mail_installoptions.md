---
title: Claws Mail installieren
description: "Nun kannst du auswälen, wo Starter für Claws Mail angelegt werden sollen. Wir wählen hier zusätzlich den Desktop an."
image: /img/claws-mail_install_windows/18_windows_claws-mail_installoptions_e.png
weight: 190
---