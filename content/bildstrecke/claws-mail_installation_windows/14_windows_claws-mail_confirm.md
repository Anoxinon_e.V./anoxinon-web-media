---
title: Claws Mail installieren
description: "Claws Mail fordert nun die Berechtigung an, die es braucht, um alles ordnungsgemäß zu installieren. Bestätige dies."
image: /img/claws-mail_install_windows/14_windows_claws-mail_confirm_e.png
weight: 150
---