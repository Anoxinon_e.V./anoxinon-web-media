---
title: Claws Mail installieren
description: "Jetzt kannst das Verzeichnis wählen, in das Claws Mail installiert wird. Die Voreinstellung passt wunderbar, gehe einfach weiter."
image: /img/claws-mail_install_windows/17_windows_claws-mail_installdir_e.png
weight: 180
---